package com.example.appseguimientoacuerdos.Core

import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.appseguimientoacuerdos.R

object Component {

    const val IS_RESPONSABLE = 1
    const val IS_NOT_RESPONSABLE = 2
    const val REUNIONES_FRAGMENT_CONTAINER = R.id.activity_home_fragment_container
    const val RETURN_CATEGORY_ON_BACK_PRESSED = true
    const val RETURN_MEETING_ON_BACK_PRESSED = false
    const val PRIORIDAD_NORMAL = 3
    const val PRIORIDAD_IMPORTANTE = 2
    const val PRIORIDAD_CRITICA = 1
    const val STATUS_PENDIENTE = 1
    const val STATUS_EN_PROGRESO = 2
    const val STATUS_TERMINADO = 3
    const val FILTRO_TODOS_LOS_ACUERDOS = 1
    const val FILTRO_EN_PROCESO = 2
    const val FILTRO_PENDIENTES = 3
    const val FILTRO_TERMINADOS = 4
    const val FILTRO_PRIORIDAD = 5

    const val FILTRO_TODOS_LOS_ACUERDOS_TEXTO = "Todos los acuerdos"
    const val FILTRO_EN_PROCESO_TEXTO = "En proceso"
    const val FILTRO_PENDIENTES_TEXTO = "Pendientes"
    const val FILTRO_TERMINADOS_TEXTO = "Terminados"
    const val FILTRO_PRIORIDAD_TEXTO = "Prioridad"





    fun showToastMessage(context: Context, message:String){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    fun loadFragmentHome(fragmentManager: FragmentManager, container:Int, fragment: Fragment) {
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(container, fragment)
        fragmentTransaction.commit()
    }

    fun replaceFragment(fragmentManager: FragmentManager, container:Int, fragment: Fragment){
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun validateSignupInput(username: String, password: String, number: String): Boolean {
        return username.isNotEmpty() && password.isNotEmpty() && number.isNotEmpty()
    }

}