package com.example.appseguimientoacuerdos.Core

import android.app.Application

class ObjPrefs:Application() {
    companion object{
        lateinit var prefs:Prefs
    }

    override fun onCreate() {
        super.onCreate()
        prefs = Prefs(applicationContext)
    }
}