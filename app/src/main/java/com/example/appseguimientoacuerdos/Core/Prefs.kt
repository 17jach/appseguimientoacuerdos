package com.example.appseguimientoacuerdos.Core

import android.content.Context

class Prefs(context: Context) {
    private val SHARED_NAME = "MySharedPreferences"
    private val SHARED_ISLOGGED = "IsLogegd"
    private val SHARED_USERID = "userId"

    private val storage = context.getSharedPreferences(SHARED_NAME, 0)

    fun saveIsLogged(isLogged: Boolean){
        storage.edit().putBoolean(SHARED_ISLOGGED, isLogged).apply()
    }
    fun getIsLogged():Boolean{
        return storage.getBoolean(SHARED_ISLOGGED, false)
    }
    fun saveUserId(userId:Int){
        storage.edit().putInt(SHARED_USERID, userId).apply()
    }
    fun getUserId():Int{
        return storage.getInt(SHARED_USERID, 0)
    }

    fun clearInfo() {
        storage.edit().clear().apply()
    }
}