package com.example.appseguimientoacuerdos.Core

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.appseguimientoacuerdos.Data.Database.FollowAgreementsDB

class RoomHelper {
    //Aquí vamos a instanciar la base de datos

    fun getDatabaseObject(context: Context): RoomDatabase {
        return Room.databaseBuilder(
            context,
            FollowAgreementsDB::class.java, "FollowAgreementsDB"
        ).build()
    }
    //Comentario de prueba 2.1.1

}