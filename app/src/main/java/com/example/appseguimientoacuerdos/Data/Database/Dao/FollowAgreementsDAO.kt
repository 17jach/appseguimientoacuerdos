package com.example.appseguimientoacuerdos.Data.Database.Dao


import androidx.room.*
import com.example.appseguimientoacuerdos.Data.Database.Entities.*
import com.example.appseguimientoacuerdos.Data.Database.Entities.Relations.AgreementWithMeeting
import com.example.appseguimientoacuerdos.Data.Database.Entities.Relations.CategoryWithUser
import com.example.appseguimientoacuerdos.Data.Database.Entities.Relations.MeetingWithCategory
import java.lang.reflect.Type

@Dao
interface FollowAgreementsDAO {

    /**
     * Login and signup operations
     *
     */

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun registrarUsuario(userEntity: UserEntity)

    @Query("SELECT * FROM user_table WHERE userId = :id")
    suspend fun getUserById(id: Int): UserEntity

    @Query("SELECT * FROM user_table WHERE userName = :user AND userPassword = :password")
    suspend fun getUserByUserAndPassword(user: String, password: String): UserEntity?

    @Query("SELECT userId FROM user_table WHERE userName = :user AND userPassword = :password")
    suspend fun getUserId(user: String, password: String): Int

    @Query("SELECT COUNT(*) FROM user_table")
    suspend fun getCountRows(): Int

    @Query("SELECT * FROM user_table WHERE userName = :user AND userPassword = :password")
    fun isUserRegistred(user: String, password: String): Boolean


    /****
     *
     * INSERT OPERATIONS
     *
     * */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMeeting(meeting: MeetingEntity):Long

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAgreement(agreement: AgreementEntity): Long

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertInvolved(involved: InvolvedEntity)

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategory(categoryEntity: CategoryEntity)


    /**
     *
     * DELETE OPERATIONS
     * REFACTOR PARA ELIMINAR RELACIONES
     * ***/
    @Query("DELETE FROM involved_table WHERE involvedId = :involvedId")
    suspend fun deleteInvolved(involvedId: Int)

    @Query("DELETE FROM meeting_table WHERE meetingId = :meetingId")
    suspend fun deleteMeeting(meetingId: Int)

    @Query("DELETE FROM category_table WHERE categoryId = :categoryId")
    suspend fun deleteCategory(categoryId: Int)


    @Query("DELETE FROM agreement_table WHERE agreementId = :agreementId")
    suspend fun deleteAgreement(agreementId: Int)

    /**
     *
     *
     * UPDATE OPERATIONS
     *
     * */
    @Update
    suspend fun updateInvolved(currentInvolved: InvolvedEntity): Int

    @Update
    suspend fun updateAcuerdoById(agreement: AgreementEntity): Int

    @Query("UPDATE user_table SET userName= :userNameHardCode WHERE userId=:userId")
    suspend fun setUserNameUpdate(userId:Int, userNameHardCode: String): Int

    @Query("UPDATE user_table set userNumber= :numberHardCode WHERE userId=:userId")
    suspend fun setNumberUpdate(userId:Int, numberHardCode: String): Int

    @Query("UPDATE user_table set userPassword= :passwordHardCode WHERE userId=:userId")
    suspend fun setPasswordUpdate(userId:Int, passwordHardCode: String): Int

    /***
     *
     *
     * TRANSACTIONS BETWEEN TABLES
     *
     * */

    @Transaction
    @Query("SELECT * FROM agreement_table JOIN meeting_table WHERE (agreementIdMeetingOwner = meetingId) AND (meetingId = :id)")
    suspend fun getAgreementWithMeetingInfo(id:Int):List<AgreementWithMeeting>

    @Transaction
    @Query("SELECT * FROM agreement_table JOIN meeting_table WHERE (agreementIdMeetingOwner = meetingId) AND (meetingId = :id) AND agreementStatusId=2")
    suspend fun getAgreementWithMeetingInfoEnProceso(id:Int):List<AgreementWithMeeting>
    @Transaction
    @Query("SELECT * FROM agreement_table JOIN meeting_table WHERE (agreementIdMeetingOwner = meetingId) AND (meetingId = :id) AND agreementStatusId=1")
    suspend fun getAgreementWithMeetingInfoPendientes(id:Int):List<AgreementWithMeeting>
    @Transaction
    @Query("SELECT * FROM agreement_table JOIN meeting_table WHERE (agreementIdMeetingOwner = meetingId) AND (meetingId = :id) AND agreementStatusId=3")
    suspend fun getAgreementWithMeetingInfoTerminados(id:Int):List<AgreementWithMeeting>
    @Transaction
    @Query("SELECT * FROM agreement_table JOIN meeting_table WHERE (agreementIdMeetingOwner = meetingId) AND (meetingId = :id) ORDER BY agreementPriorityId")
    suspend fun getAgreementWithMeetingInfoPrioridad(id:Int):List<AgreementWithMeeting>




    @Transaction
    @Query("SELECT * FROM category_table WHERE categoryIdUserOwner = :userId")
    suspend fun getCategoryFromUser(userId: Int): List<CategoryWithUser>

    @Transaction
    @Query("SELECT * FROM meeting_table WHERE meetingIdCategoryOwner = :categoryId")
    suspend fun getMeetingFromCategory(categoryId:Int):List<MeetingWithCategory>

    @Transaction
    @Query("SELECT * FROM involved_table WHERE involvedMeetingOwnerId = :idMeetingOwner")
    suspend fun getAllInvolvedByMeeting(idMeetingOwner: Int): List<InvolvedEntity>

    @Query("SELECT * FROM involved_table WHERE involvedId = :involvedId")
    suspend fun getInvolvedById(involvedId: Int): InvolvedEntity


    @Query("SELECT * FROM agreement_table WHERE agreementId = :id")
    suspend fun getAgreementById(id: Int): AgreementEntity

    @Query("SELECT * FROM agreement_table")
    suspend fun getAllAgreement(): List<AgreementEntity>

    @Query("SELECT * FROM agreement_table WHERE agreementIdMeetingOwner = :idMeetingOwner")
    suspend fun getAgreementByMeeting(idMeetingOwner: Int): List<AgreementEntity>


    /**
     *
     * Tables of references
     *
     * */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPriority(priorityEntity: PriorityEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertStatus(statusEntity: StatusEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertType(typeEntity: TypeEntity)


    /*INIT DATA TABLES*/
    @Query("SELECT * FROM priorityentity")
    suspend fun getAllPriority(): List<PriorityEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun initPriorityTable(priorityData: ArrayList<PriorityEntity>)

    @Query("SELECT * FROM statusentity")
    suspend fun getAllStatus(): List<StatusEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun initStatusTable(statusData: ArrayList<StatusEntity>)

    @Query("SELECT * FROM typeentity")
    suspend fun getAllType(): List<TypeEntity>

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    suspend fun initTypeTable(typeData: ArrayList<TypeEntity>)

    @Query("SELECT * FROM involved_table WHERE involvedMeetingOwnerId = :idMeetingOwner")
    suspend fun getInvolvedByMeeting(idMeetingOwner: Int): List<InvolvedEntity>

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    suspend fun insertResponsable(responsableEntity: ResponsableEntity):Long

    @Query("SELECT * FROM responsable_table WHERE responsableIdAgreementOwner = :id")
    suspend fun getResponsableByAgreement(id: Int):List<ResponsableEntity>
}