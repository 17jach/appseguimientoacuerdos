package com.example.appseguimientoacuerdos.Data.Database.Entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "agreement_table")
data class AgreementEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "agreementId") val idAgreement: Int,
    @ColumnInfo(name = "agreementNumber") val numberAgreement: String,
    @ColumnInfo(name = "agreementDescripcion") val description: String,
    @ColumnInfo(name = "agreementCreateDate") val createDate: String,
    @ColumnInfo(name = "agreementFinishDate") val finishDate: String,
    @ColumnInfo(name = "agreementIdMeetingOwner") val idMeetingOwner:Int,
    @ColumnInfo(name = "agreementPriorityId") val priority: Int,
    @ColumnInfo(name = "agreementStatusId") val status: Int
)
