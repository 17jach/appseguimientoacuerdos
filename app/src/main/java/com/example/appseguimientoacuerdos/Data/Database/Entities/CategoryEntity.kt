package com.example.appseguimientoacuerdos.Data.Database.Entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "category_table")
data class CategoryEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "categoryId") val categoryId:Int,
    @ColumnInfo(name = "categoryTitle") val title:String,
    @ColumnInfo(name = "categoryIdUserOwner") val idUserOwner:Int
)
