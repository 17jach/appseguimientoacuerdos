package com.example.appseguimientoacuerdos.Data.Database.Entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "involved_table")
data class InvolvedEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "involvedId") val idInvolved: Int,
    @ColumnInfo(name = "involvedName") val involvedName: String,
    @ColumnInfo(name = "involvedPhoneNumber") val phoneNumber: String,
    @ColumnInfo(name = "involvedMeetingOwnerId") val idMeetingOwnerInvolved: Int,
    )
