package com.example.appseguimientoacuerdos.Data.Database.Entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "meeting_table")
data class MeetingEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "meetingId") val idMeeting:Int,
    @ColumnInfo(name = "meetingTitle") val title:String,
    @ColumnInfo(name = "meetingOrder") val order:String,
    @ColumnInfo(name = "meetingDate") val date:String,
    @ColumnInfo(name = "meetingIdCategoryOwner") val idCategoryOwner:Int
)