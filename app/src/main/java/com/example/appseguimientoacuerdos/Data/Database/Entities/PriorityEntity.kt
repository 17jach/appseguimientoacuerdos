package com.example.appseguimientoacuerdos.Data.Database.Entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PriorityEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "priorityId") val idPriority:Int,
    @ColumnInfo(name = "priorityDescription") val descriptionPriority:String

)
