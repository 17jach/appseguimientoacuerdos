package com.example.appseguimientoacuerdos.Data.Database.Entities.Relations

import androidx.room.Embedded
import androidx.room.Relation
import com.example.appseguimientoacuerdos.Data.Database.Entities.AgreementEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.MeetingEntity

data class AgreementWithMeeting(
    @Embedded val agreement:AgreementEntity,
    @Relation(
        parentColumn = "agreementIdMeetingOwner",
        entityColumn = "meetingId"
    )
    val meetingEntity: MeetingEntity
)