package com.example.appseguimientoacuerdos.Data.Database.Entities.Relations

import androidx.room.Embedded
import androidx.room.Relation
import com.example.appseguimientoacuerdos.Data.Database.Entities.AgreementEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.PriorityEntity

data class AgreementWithPriority(
    @Embedded val agreement:AgreementEntity,
    @Relation(
        parentColumn = "agreementPriorityId",
        entityColumn = "priorityId"
    )
    val priority: PriorityEntity
)