package com.example.appseguimientoacuerdos.Data.Database.Entities.Relations

import androidx.room.Embedded
import androidx.room.Relation
import com.example.appseguimientoacuerdos.Data.Database.Entities.AgreementEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.StatusEntity

data class AgreementWithStatus(
    @Embedded val agreement:AgreementEntity,
    @Relation(
        parentColumn = "agreementStatusId",
        entityColumn = "statusId"
    )
    val status:StatusEntity
)