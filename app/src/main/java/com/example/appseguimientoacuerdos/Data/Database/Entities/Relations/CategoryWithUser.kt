package com.example.appseguimientoacuerdos.Data.Database.Entities.Relations

import androidx.room.Embedded
import androidx.room.Relation
import com.example.appseguimientoacuerdos.Data.Database.Entities.CategoryEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.MeetingEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.UserEntity


data class CategoryWithUser(
    @Embedded val categoryEntity: CategoryEntity,
    @Relation(
        parentColumn = "categoryIdUserOwner",
        entityColumn = "userId"
    )
    val user:UserEntity
)
