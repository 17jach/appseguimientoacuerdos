package com.example.appseguimientoacuerdos.Data.Database.Entities.Relations

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.appseguimientoacuerdos.Data.Database.Entities.AgreementEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.InvolvedEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.MeetingEntity

data class InvolvedWithMeeting(
    @Embedded val involved:InvolvedEntity,
    @Relation(
        parentColumn = "involvedId",
        entityColumn = "meetingId"
    )
    val meetingEntity: MeetingEntity

)