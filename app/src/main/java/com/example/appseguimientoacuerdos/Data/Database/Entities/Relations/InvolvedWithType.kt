package com.example.appseguimientoacuerdos.Data.Database.Entities.Relations

import androidx.room.Embedded
import androidx.room.Relation
import com.example.appseguimientoacuerdos.Data.Database.Entities.InvolvedEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.TypeEntity

data class InvolvedWithType(
    @Embedded val involved:InvolvedEntity,
    @Relation(
        parentColumn = "involvedTypeId",
        entityColumn = "typeId"
    )
    val type:TypeEntity
)