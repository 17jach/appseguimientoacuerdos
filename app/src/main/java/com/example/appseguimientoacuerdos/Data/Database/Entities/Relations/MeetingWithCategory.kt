package com.example.appseguimientoacuerdos.Data.Database.Entities.Relations

import androidx.room.Embedded
import androidx.room.Relation
import com.example.appseguimientoacuerdos.Data.Database.Entities.CategoryEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.MeetingEntity

data class MeetingWithCategory(
    @Embedded val meetingEntity: MeetingEntity,
    @Relation(
        parentColumn = "meetingIdCategoryOwner",
        entityColumn = "categoryId"
    )
    val categoryEntity: CategoryEntity
)
