package com.example.appseguimientoacuerdos.Data.Database.Entities.Relations

import androidx.room.Embedded
import androidx.room.Relation
import com.example.appseguimientoacuerdos.Data.Database.Entities.AgreementEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.ResponsableEntity

data class ResponsableWithAgreement(
    @Embedded val responsableEntity: ResponsableEntity,
    @Relation(
        parentColumn = "responsableIdAgreementOwner",
        entityColumn = "agreementId"
    ) val agreementEntity: AgreementEntity
)
