package com.example.appseguimientoacuerdos.Data.Database.Entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "responsable_table")
data class ResponsableEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "responsableId") val idResponsable: Int,
    @ColumnInfo(name = "responsableName") val responsableName: String,
    @ColumnInfo(name = "responsableIdAgreementOwner") val idAgreementOwnerInvolved: Int,
)
