package com.example.appseguimientoacuerdos.Data.Database.Entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class StatusEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "statusId") val idStatus:Int,
    @ColumnInfo(name = "statusDescription") val descriptionStatus:String
)