package com.example.appseguimientoacuerdos.Data.Database.Entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TypeEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "typeId") val idType:Int,
    @ColumnInfo(name = "typeDescription") val descriptionType:String

)