package com.example.appseguimientoacuerdos.Data.Database.Entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_table")
data class UserEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="userId") val userId: Int =0,
    @ColumnInfo(name = "userName") val name: String,
    @ColumnInfo(name = "userNumber") val number: String,
    @ColumnInfo(name = "userPassword") val password: String
)