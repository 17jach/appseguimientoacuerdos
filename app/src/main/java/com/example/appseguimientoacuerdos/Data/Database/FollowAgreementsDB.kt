package com.example.appseguimientoacuerdos.Data.Database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.appseguimientoacuerdos.Data.Database.Dao.FollowAgreementsDAO
import com.example.appseguimientoacuerdos.Data.Database.Entities.*

@Database(
    entities = [
        UserEntity::class,
        AgreementEntity::class,
        InvolvedEntity::class,
        MeetingEntity::class,
        ResponsableEntity::class,
        CategoryEntity::class,
        PriorityEntity::class,
        StatusEntity::class,
        TypeEntity::class
    ],
    version = 3
)
abstract class FollowAgreementsDB : RoomDatabase() {
    abstract fun followAgreementsDao(): FollowAgreementsDAO
    //Commit para continuar con el flow database
    companion object{
        @Volatile
        private var INSTANCE:FollowAgreementsDB?=null
        fun getDatabase(context: Context):FollowAgreementsDB{
            return INSTANCE ?: synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FollowAgreementsDB::class.java,
                    "follow_agreements_db"
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}