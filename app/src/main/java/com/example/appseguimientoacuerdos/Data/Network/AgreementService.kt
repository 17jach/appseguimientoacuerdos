package com.example.appseguimientoacuerdos.Data.Network

import com.example.appseguimientoacuerdos.Data.Database.Dao.FollowAgreementsDAO
import com.example.appseguimientoacuerdos.Data.Database.Entities.*
import com.example.appseguimientoacuerdos.Data.Database.Entities.Relations.AgreementWithMeeting
import com.example.appseguimientoacuerdos.Data.Repository.FollowAgreementRepository
import com.example.appseguimientoacuerdos.Data.Repository.Response.AgreementRecyclerResponse

class AgreementService(followAgreementsDAO: FollowAgreementsDAO) {
    private val repositoryFollowAgreement = FollowAgreementRepository(followAgreementsDAO)


    suspend fun getAllPriorityData(): List<PriorityEntity> {
        return repositoryFollowAgreement.getAllPriorityData()
    }

    suspend fun getAllStatusData(): List<StatusEntity> {
        return repositoryFollowAgreement.getAllStatusData()
    }

    suspend fun insertAcuerdo(
        acuerdo: AgreementEntity
    ): Int {
        return repositoryFollowAgreement.insertAcuerdo(acuerdo)
    }

    suspend fun getAgreementById(id: Int): AgreementEntity {
        return repositoryFollowAgreement.getAgreementById(id)
    }

    suspend fun updateAcuerdoById(agreement: AgreementEntity): Int {
        return repositoryFollowAgreement.updateAcuerdoById(agreement)
    }

    suspend fun getAgreementByMeeting(idMeetingOwner: Int): List<AgreementEntity> {
        return repositoryFollowAgreement.getAgreementByMeeting(idMeetingOwner)
    }

    /*FILTROS*/
    suspend fun getAgreementWithMeetingInfo(id:Int): List<AgreementWithMeeting> {
        return repositoryFollowAgreement.getAgreementWithMeetingInfo(id)
    }
    suspend fun getAgreementWithMeetingInfoEnProceso(id:Int): List<AgreementWithMeeting> {
        return repositoryFollowAgreement.getAgreementWithMeetingInfoEnProceso(id)
    }
    suspend fun getAgreementWithMeetingInfoPendientes(id:Int): List<AgreementWithMeeting> {
        return repositoryFollowAgreement.getAgreementWithMeetingInfoPendientes(id)
    }
    suspend fun getAgreementWithMeetingInfoTerminados(id:Int): List<AgreementWithMeeting> {
        return repositoryFollowAgreement.getAgreementWithMeetingInfoTerminados(id)
    }
    suspend fun getAgreementWithMeetingInfoPrioridad(id:Int): List<AgreementWithMeeting> {
        return repositoryFollowAgreement.getAgreementWithMeetingInfoPrioridad(id)
    }



    suspend fun deleteAgreement(agreementId: Int) {
        return repositoryFollowAgreement.deleteAgreement(agreementId)
    }

    suspend fun getInvolvedByMeeting(idMeetingOwner: Int): List<InvolvedEntity> {
        return repositoryFollowAgreement.getInvolvedByMeeting(idMeetingOwner)
    }

    suspend fun insertResponsable(responsableEntity: ResponsableEntity): Long {
        return repositoryFollowAgreement.insertResponsable(responsableEntity)
    }

    suspend fun getResponsableByAgreement(id: Int): List<ResponsableEntity> {
        return repositoryFollowAgreement.getResponsableByAgreement(id)
    }
}