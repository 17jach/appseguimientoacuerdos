package com.example.appseguimientoacuerdos.Data.Network

import com.example.appseguimientoacuerdos.Data.Database.Dao.FollowAgreementsDAO
import com.example.appseguimientoacuerdos.Data.Database.Entities.CategoryEntity
import com.example.appseguimientoacuerdos.Data.Repository.FollowAgreementRepository

class CategoryService(followAgreementsDao: FollowAgreementsDAO) {
    private val followAgreementRepository = FollowAgreementRepository(followAgreementsDao)

    suspend fun getCategoryList(userId: Int): List<CategoryEntity> {
        return followAgreementRepository.getCategorysFromUserId(userId)
    }

    suspend fun agregarCategoria(categoryEntity: CategoryEntity) {
        return followAgreementRepository.insertCategory(categoryEntity)
    }

    suspend fun deleteCategory(categoryId: Int) {
        return followAgreementRepository.deleteCategory(categoryId)
    }

}