package com.example.appseguimientoacuerdos.Data.Network

import com.example.appseguimientoacuerdos.Data.Database.Dao.FollowAgreementsDAO
import com.example.appseguimientoacuerdos.Data.Database.Entities.UserEntity
import com.example.appseguimientoacuerdos.Data.Repository.FollowAgreementRepository

class InformationService(followAgreementsDao: FollowAgreementsDAO) {
    private val repositoryFollowAgreement = FollowAgreementRepository(followAgreementsDao)

    suspend fun getUserById(id: Int): UserEntity {
        return repositoryFollowAgreement.getUserById(id)
    }

    suspend fun setUserNameUpdate(userNameHardCode: String, userId: Int): Int {
        return repositoryFollowAgreement.setUserNameUpdate(userNameHardCode,userId)
    }

    suspend fun setNumberUpdate(numberHardCode: String, userId: Int): Int {
        return repositoryFollowAgreement.setNumberUpdate(numberHardCode, userId)
    }

    suspend fun setPasswordUpdate(passwordHardCode: String, userId: Int): Int {
        return repositoryFollowAgreement.setPasswordUpdate(passwordHardCode, userId)
    }
}