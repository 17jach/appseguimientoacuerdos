package com.example.appseguimientoacuerdos.Data.Network

import com.example.appseguimientoacuerdos.Data.Database.Dao.FollowAgreementsDAO
import com.example.appseguimientoacuerdos.Data.Database.Entities.InvolvedEntity
import com.example.appseguimientoacuerdos.Data.Repository.FollowAgreementRepository

class InvolvedService(followAgreementsDAO: FollowAgreementsDAO) {
    private val repositoryFollowAgreement = FollowAgreementRepository(followAgreementsDAO)

    suspend fun getAllInvolvedByMeeting(idMeetingOwner: Int): List<InvolvedEntity> {
        return repositoryFollowAgreement.getInvolvedByMeeting(idMeetingOwner)
    }

    suspend fun insertInvolved(involvedEntity: InvolvedEntity) {
        return repositoryFollowAgreement.insertarInvolved(involvedEntity)
    }

    suspend fun deleteInvolved(involvedId: Int) {
        return repositoryFollowAgreement.deleteInvolved(involvedId)
    }

    suspend fun getInvolvedById(involvedId: Int): InvolvedEntity {
        return repositoryFollowAgreement.getInvolvedById(involvedId)
    }

    suspend fun updateInvolved(currentInvolved: InvolvedEntity): Int {
        return repositoryFollowAgreement.updateInvolved(currentInvolved)
    }

}