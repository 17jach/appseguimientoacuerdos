package com.example.appseguimientoacuerdos.Data.Network

import com.example.appseguimientoacuerdos.Data.Database.Dao.FollowAgreementsDAO
import com.example.appseguimientoacuerdos.Data.Database.Entities.*
import com.example.appseguimientoacuerdos.Data.Repository.FollowAgreementRepository
import com.example.appseguimientoacuerdos.Data.Repository.Response.AgreementRecyclerResponse

class LoginService(followAgreementsDao:FollowAgreementsDAO) {
    val repositoryFollowAgreement: FollowAgreementRepository =
        FollowAgreementRepository(followAgreementsDao)


    suspend fun authUser(user: String, password: String): UserEntity? {
        return repositoryFollowAgreement.authUser(user, password)
    }

    suspend fun getUserId(user: String, password: String): Int {
        return repositoryFollowAgreement.getUserId(user, password)
    }

    suspend fun initPriorityTable() {
        return repositoryFollowAgreement.initPriorityTable()
    }

    suspend fun initStatusTable() {
        return repositoryFollowAgreement.initStatusTable()
    }

    suspend fun initTypeTable() {
        return repositoryFollowAgreement.initTypeTable()
    }

    /************************/



}

