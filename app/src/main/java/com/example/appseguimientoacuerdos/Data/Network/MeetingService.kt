package com.example.appseguimientoacuerdos.Data.Network

import com.example.appseguimientoacuerdos.Data.Database.Dao.FollowAgreementsDAO
import com.example.appseguimientoacuerdos.Data.Database.Entities.MeetingEntity
import com.example.appseguimientoacuerdos.Data.Repository.FollowAgreementRepository

class MeetingService(followAgreementsDao: FollowAgreementsDAO) {
    private val repositoryFollowAgreement = FollowAgreementRepository(followAgreementsDao)

    suspend fun insertarReunion(meeting: MeetingEntity): Long {
        return repositoryFollowAgreement.insertarReunion(meeting)
    }

    suspend fun getMeetingFromCategoryId(categoryId:Int): List<MeetingEntity> {
        return repositoryFollowAgreement.getMeetingsFromCategoryId(categoryId)
    }

    suspend fun deleteMeeting(meetingId: Int) {
        return repositoryFollowAgreement.deleteMeeting(meetingId)
    }
}