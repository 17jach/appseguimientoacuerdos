package com.example.appseguimientoacuerdos.Data.Network

import com.example.appseguimientoacuerdos.Data.Database.Dao.FollowAgreementsDAO
import com.example.appseguimientoacuerdos.Data.Database.Entities.UserEntity
import com.example.appseguimientoacuerdos.Data.Repository.FollowAgreementRepository

class SignupService(followAgreementsDao:FollowAgreementsDAO) {
    private val repositoryFollowAgreement = FollowAgreementRepository(followAgreementsDao)
    suspend fun registrarUsuario(user: UserEntity) {
        return repositoryFollowAgreement.registrarUsuario(user)
    }

    suspend fun isUserRegistred(user: UserEntity): Boolean {
        return repositoryFollowAgreement.isUserRegistred(user)
    }
}