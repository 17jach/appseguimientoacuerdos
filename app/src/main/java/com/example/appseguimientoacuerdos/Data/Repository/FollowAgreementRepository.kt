package com.example.appseguimientoacuerdos.Data.Repository

import com.example.appseguimientoacuerdos.Data.Database.Dao.FollowAgreementsDAO
import com.example.appseguimientoacuerdos.Data.Database.Entities.*
import com.example.appseguimientoacuerdos.Data.Database.Entities.Relations.AgreementWithMeeting
import com.example.appseguimientoacuerdos.Data.Database.Entities.Relations.CategoryWithUser
import com.example.appseguimientoacuerdos.Data.Database.Entities.Relations.MeetingWithCategory
import com.example.appseguimientoacuerdos.Data.Repository.Response.AgreementRecyclerResponse

class FollowAgreementRepository(private val followAgreementsDao: FollowAgreementsDAO) {

    /******
     * Signup functions
     */

    suspend fun isUserRegistred(user: UserEntity): Boolean {
        return followAgreementsDao.isUserRegistred(user.name, user.password)
    }


    suspend fun registrarUsuario(user: UserEntity) {
        if (followAgreementsDao.getCountRows() == 0) {
            followAgreementsDao.registrarUsuario(user)
        } else {
            if (!isUserRegistred(user)) {
                followAgreementsDao.registrarUsuario(user)
            }
        }
    }

    /******
     * Login functions
     */

    suspend fun getUserById(id: Int): UserEntity {
        val userById: UserEntity = followAgreementsDao.getUserById(id)
        return userById
    }

    suspend fun authUser(user: String, password: String): UserEntity? {
        return followAgreementsDao.getUserByUserAndPassword(user, password)
    }

    suspend fun getUserId(user: String, password: String): Int {
        return followAgreementsDao.getUserId(user, password)
    }


    /**
     * Category functions
     */

    suspend fun deleteCategory(categoryId: Int) {
        followAgreementsDao.deleteCategory(categoryId)
    }

    suspend fun insertCategory(category: CategoryEntity) {
        followAgreementsDao.insertCategory(category)
    }

    suspend fun getCategorysFromUserId(userId: Int): List<CategoryEntity> {
        //This code should be on UseCase
        val categoryList = ArrayList<CategoryEntity>()
        val listCategoryWithUsers: List<CategoryWithUser> =
            followAgreementsDao.getCategoryFromUser(userId)
        for (categoryInfo in listCategoryWithUsers) {
            categoryList.add(categoryInfo.categoryEntity)
        }
        return categoryList
    }


    /******
     * Meeting functions
     */
    suspend fun deleteMeeting(meetingId: Int) {
        followAgreementsDao.deleteMeeting(meetingId)

    }

    suspend fun insertarReunion(meeting: MeetingEntity): Long {
        return followAgreementsDao.insertMeeting(meeting)
    }

    suspend fun getMeetingsFromCategoryId(categoryId: Int): List<MeetingEntity> {
        val meetingList = ArrayList<MeetingEntity>()
        val listMeetingWithCategory: List<MeetingWithCategory> =
            followAgreementsDao.getMeetingFromCategory(categoryId)
        for (meetingInfo in listMeetingWithCategory) {
            meetingList.add(meetingInfo.meetingEntity)
        }
        return meetingList
    }

    /******
     *
     *
     * Involved functinos
     *
     */


    suspend fun insertarInvolved(involvedEntity: InvolvedEntity) {
        followAgreementsDao.insertInvolved(involvedEntity)
    }

    suspend fun getInvolvedByMeeting(idMeetingOwner: Int): List<InvolvedEntity> {
        return followAgreementsDao.getAllInvolvedByMeeting(idMeetingOwner)
    }


    suspend fun deleteInvolved(involvedId: Int) {
        followAgreementsDao.deleteInvolved(involvedId)
    }

    suspend fun getInvolvedById(involvedId: Int): InvolvedEntity {
        return followAgreementsDao.getInvolvedById(involvedId)
    }

    suspend fun updateInvolved(currentInvolved: InvolvedEntity): Int {
        return followAgreementsDao.updateInvolved(currentInvolved)
    }


    /***
     *
     * Agreement functions
     *
     * ****/


    suspend fun getAgreementByMeeting(idMeetingOwner: Int): List<AgreementEntity> {
        return followAgreementsDao.getAgreementByMeeting(idMeetingOwner)
    }


    suspend fun insertAcuerdo(acuerdo: AgreementEntity): Int {
        val id = followAgreementsDao.insertAgreement(acuerdo)
        return id.toInt()
    }

    suspend fun getAgreementById(id: Int): AgreementEntity {
        return followAgreementsDao.getAgreementById(id)
    }
/*FILTROS*/
    suspend fun getAgreementWithMeetingInfo(id: Int): List<AgreementWithMeeting> {
        return followAgreementsDao.getAgreementWithMeetingInfo(id)
    }
    suspend fun getAgreementWithMeetingInfoEnProceso(id: Int): List<AgreementWithMeeting> {
        return followAgreementsDao.getAgreementWithMeetingInfoEnProceso(id)
    }
    suspend fun getAgreementWithMeetingInfoPendientes(id: Int): List<AgreementWithMeeting> {
        return followAgreementsDao.getAgreementWithMeetingInfoPendientes(id)
    }
    suspend fun getAgreementWithMeetingInfoTerminados(id: Int): List<AgreementWithMeeting> {
        return followAgreementsDao.getAgreementWithMeetingInfoTerminados(id)
    }
    suspend fun getAgreementWithMeetingInfoPrioridad(id: Int): List<AgreementWithMeeting> {
        return followAgreementsDao.getAgreementWithMeetingInfoPrioridad(id)
    }


    suspend fun updateAcuerdoById(agreement: AgreementEntity): Int {
        val idUpdated = followAgreementsDao.updateAcuerdoById(agreement)
        return idUpdated
    }

    suspend fun deleteAgreement(agreementId: Int) {
        followAgreementsDao.deleteAgreement(agreementId)
    }


    /**
     *
     * Generic Tables
     *
     * *
     *
     * **/

    suspend fun getAllPriorityData(): List<PriorityEntity> {
        return followAgreementsDao.getAllPriority()
    }

    suspend fun getAllStatusData(): List<StatusEntity> {
        return followAgreementsDao.getAllStatus()
    }

    suspend fun initPriorityTable() {
        val priorityList: List<PriorityEntity> = followAgreementsDao.getAllPriority()
        if (priorityList.isEmpty()) {
            val priorityData = arrayListOf<PriorityEntity>()
            priorityData.add(PriorityEntity(1, "Critica"))
            priorityData.add(PriorityEntity(2, "Importante"))
            priorityData.add(PriorityEntity(3, "Normal"))

            followAgreementsDao.initPriorityTable(priorityData)
        }
    }

    suspend fun initStatusTable() {
        val statusList: List<StatusEntity> = followAgreementsDao.getAllStatus()
        if (statusList.isEmpty()) {
            val statusData = arrayListOf<StatusEntity>()
            statusData.add(StatusEntity(1, "Pendiente"))
            statusData.add(StatusEntity(2, "En progreso"))
            statusData.add(StatusEntity(3, "Terminado"))

            followAgreementsDao.initStatusTable(statusData)
        }
    }

    suspend fun initTypeTable() {
        val typeList: List<TypeEntity> = followAgreementsDao.getAllType()
        if (typeList.isEmpty()) {
            val typeData = arrayListOf<TypeEntity>()
            typeData.add(TypeEntity(1, "Responsable"))
            typeData.add(TypeEntity(2, "Integrante"))

            followAgreementsDao.initTypeTable(typeData)
        }
    }

    suspend fun setUserNameUpdate(userNameHardCode: String, userId: Int): Int {
        return followAgreementsDao.setUserNameUpdate(userId, userNameHardCode)
    }

    suspend fun setNumberUpdate(numberHardCode: String, userId: Int): Int {
        return followAgreementsDao.setNumberUpdate(userId, numberHardCode)
    }

    suspend fun setPasswordUpdate(passwordHardCode: String, userId: Int): Int {
        return followAgreementsDao.setPasswordUpdate(userId, passwordHardCode)
    }

    suspend fun insertResponsable(responsableEntity: ResponsableEntity): Long {
        return followAgreementsDao.insertResponsable(responsableEntity)
    }

    suspend fun getResponsableByAgreement(id: Int): List<ResponsableEntity> {
        return followAgreementsDao.getResponsableByAgreement(id)
    }

}