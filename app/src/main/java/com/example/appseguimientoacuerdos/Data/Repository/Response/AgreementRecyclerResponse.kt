package com.example.appseguimientoacuerdos.Data.Repository.Response

data class AgreementRecyclerResponse(
    val idAgreement: Int,
    val nameAgreement: String,
    val description: String,
    val finishDate: String,
    val priority: Int,
    val status: Int,
    val titleMeeting:String
)