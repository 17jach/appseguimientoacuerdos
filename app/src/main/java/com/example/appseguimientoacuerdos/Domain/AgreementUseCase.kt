package com.example.appseguimientoacuerdos.Domain

import android.app.Application
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Data.Database.Entities.*
import com.example.appseguimientoacuerdos.Data.Database.Entities.Relations.AgreementWithMeeting
import com.example.appseguimientoacuerdos.Data.Database.FollowAgreementsDB
import com.example.appseguimientoacuerdos.Data.Network.AgreementService
import com.example.appseguimientoacuerdos.Data.Network.LoginService
import com.example.appseguimientoacuerdos.Data.Repository.Response.AgreementRecyclerResponse

class AgreementUseCase(application: Application) {
    private val dao = FollowAgreementsDB.getDatabase(application.applicationContext).followAgreementsDao()
    private val service: AgreementService = AgreementService(dao)

    suspend fun getAllPriorityData(): List<PriorityEntity> {
        return service.getAllPriorityData()
    }

    suspend fun getAllStatusData(): List<StatusEntity> {
        return service.getAllStatusData()
    }

    suspend fun getResponsableByAgreement(id:Int): List<ResponsableEntity> {
               return service.getResponsableByAgreement(id)
    }

    suspend fun insertAcuerdo(
        acuerdo: AgreementEntity
    ): Int {
        return service.insertAcuerdo(acuerdo)
    }

    suspend fun getAgreementById(id: Int): AgreementEntity {
        return service.getAgreementById(id)
    }

    suspend fun updateAcuerdoById(agreement: AgreementEntity): Int {
        return service.updateAcuerdoById(agreement)
    }

    suspend fun getAgreementByMeeting(idMeetingOwner: Int): List<AgreementEntity> {
        return service.getAgreementByMeeting(idMeetingOwner)
    }

    suspend fun getAgreementWithMeetingServiceCall(id:Int, filtro:Int): List<AgreementWithMeeting> {
        var response = emptyList<AgreementWithMeeting>()

        when(filtro){
            Component.FILTRO_TODOS_LOS_ACUERDOS->{
                response = service.getAgreementWithMeetingInfo(id)
            }
            Component.FILTRO_EN_PROCESO->{
                response = service.getAgreementWithMeetingInfoEnProceso(id)
            }
            Component.FILTRO_PENDIENTES->{
                response = service.getAgreementWithMeetingInfoPendientes(id)
            }
            Component.FILTRO_TERMINADOS->{
                response = service.getAgreementWithMeetingInfoTerminados(id)
            }
            Component.FILTRO_PRIORIDAD->{
                response = service.getAgreementWithMeetingInfoPrioridad(id)
            }

        }
        return response
    }

    suspend fun getAgreementWithMeetingInfo(id:Int, filtro:Int): List<AgreementRecyclerResponse> {
        val response = getAgreementWithMeetingServiceCall(id, filtro)

        val listResult = arrayListOf<AgreementRecyclerResponse>()
        for(res in response){
            val result = AgreementRecyclerResponse(
                res.agreement.idAgreement,
                res.agreement.numberAgreement,
                res.agreement.description,
                res.agreement.finishDate,
                res.agreement.priority,
                res.agreement.status,
                res.meetingEntity.title
            )
            listResult.add(result)
        }
        return listResult
    }

    suspend fun deleteAgreement(agreementId: Int) {
        return service.deleteAgreement(agreementId)
    }

    suspend fun getInvolvedByMeeting(idMeetingOwner: Int): List<InvolvedEntity> {
        return service.getInvolvedByMeeting(idMeetingOwner)
    }

    suspend fun insertResponsable(responsableEntity: ResponsableEntity): Long {
        return service.insertResponsable(responsableEntity)
    }

}