package com.example.appseguimientoacuerdos.Domain

import android.app.Application
import com.example.appseguimientoacuerdos.Data.Database.Entities.CategoryEntity
import com.example.appseguimientoacuerdos.Data.Database.FollowAgreementsDB
import com.example.appseguimientoacuerdos.Data.Network.CategoryService

class CategoryUseCase(application: Application) {


    val dao = FollowAgreementsDB.getDatabase(application.applicationContext).followAgreementsDao()
    val service = CategoryService(dao)

    suspend fun getCategoryList(userId: Int): List<CategoryEntity> {
        return service.getCategoryList(userId)
    }

    suspend fun agregarCategoria(tituloCategoria: String, idUserOwner:Int) {
        val categoryEntity = CategoryEntity(0, tituloCategoria, idUserOwner)
        return service.agregarCategoria(categoryEntity)
    }

    suspend fun deleteCategory(categoryId: Int) {
        return service.deleteCategory(categoryId)
    }

}