package com.example.appseguimientoacuerdos.Domain

import android.app.Application
import com.example.appseguimientoacuerdos.Data.Database.FollowAgreementsDB
import com.example.appseguimientoacuerdos.Data.Network.InformationService

class InformationUseCase (application: Application) {
    val dao = FollowAgreementsDB.getDatabase(application.applicationContext).followAgreementsDao()
    val service = InformationService(dao)

    suspend fun getUserName(id:Int): String {
        val name = service.getUserById(id).name
        return name
    }

    suspend fun getNumber(id: Int): String {
        val number = service.getUserById(id).number
        return number
    }

    suspend fun getPassword(id: Int): String {
        val password = service.getUserById(id).password
        return password
    }

    suspend fun setUserNameUpdate(userNameHardCode: String, userId: Int) {
        service.setUserNameUpdate(userNameHardCode,userId)
    }

    suspend fun setNumberUpdate(numberHardCode: String, userId: Int) {
        print(numberHardCode)
        service.setNumberUpdate(numberHardCode, userId)
    }

    suspend fun setPasswordUpdate(passwordHardCode: String, userId: Int) {
        service.setPasswordUpdate(passwordHardCode, userId)
    }
}