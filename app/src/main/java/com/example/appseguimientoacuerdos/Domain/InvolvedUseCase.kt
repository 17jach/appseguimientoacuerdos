package com.example.appseguimientoacuerdos.Domain

import android.app.Application
import com.example.appseguimientoacuerdos.Data.Database.Entities.InvolvedEntity
import com.example.appseguimientoacuerdos.Data.Database.FollowAgreementsDB
import com.example.appseguimientoacuerdos.Data.Network.InvolvedService

class InvolvedUseCase(application: Application) {
    val dao = FollowAgreementsDB.getDatabase(application.applicationContext).followAgreementsDao()
    val service: InvolvedService = InvolvedService(dao)

    suspend fun getAllInvolvedByMeeting(idMeetingOwner: Int): List<InvolvedEntity> {
        return service.getAllInvolvedByMeeting(idMeetingOwner)
    }

    suspend fun insertInvolved(involvedEntity: InvolvedEntity){
        return service.insertInvolved(involvedEntity)
    }

    suspend fun deleteInvolved(involvedId: Int) {
        return service.deleteInvolved(involvedId)
    }

    suspend fun getInvolvedById(involvedId: Int):InvolvedEntity {
        return service.getInvolvedById(involvedId)
    }

    suspend fun updateInvolved(currentInvolved: InvolvedEntity): Int {
        return service.updateInvolved(currentInvolved)
    }

}