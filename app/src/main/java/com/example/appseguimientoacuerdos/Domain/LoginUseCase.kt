package com.example.appseguimientoacuerdos.Domain

import android.app.Application
import com.example.appseguimientoacuerdos.Data.Database.Entities.UserEntity
import com.example.appseguimientoacuerdos.Data.Database.FollowAgreementsDB
import com.example.appseguimientoacuerdos.Data.Network.LoginService

class LoginUseCase(application: Application) {
    val dao = FollowAgreementsDB.getDatabase(application.applicationContext).followAgreementsDao()
    val serviceLogin:LoginService = LoginService(dao)


    suspend fun authUser(user: String, password: String): Boolean {
        val userInput:UserEntity? = serviceLogin.authUser(user, password)
        return userInput != null
    }

    suspend fun getUserId(user:String, password:String): Int {
        return serviceLogin.getUserId(user, password)
    }

    suspend fun initPriorityTable() {
        return serviceLogin.initPriorityTable()
    }

    suspend fun initStatusTable() {
        return serviceLogin.initStatusTable()
    }

    suspend fun initTypeTable(){
        return serviceLogin.initTypeTable()
    }
}