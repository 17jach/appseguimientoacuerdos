package com.example.appseguimientoacuerdos.Domain

import android.app.Application
import com.example.appseguimientoacuerdos.Data.Database.Entities.MeetingEntity
import com.example.appseguimientoacuerdos.Data.Database.FollowAgreementsDB
import com.example.appseguimientoacuerdos.Data.Network.MeetingService

class MeetingUseCase(application: Application) {
    val dao = FollowAgreementsDB.getDatabase(application.applicationContext).followAgreementsDao()
    val service = MeetingService(dao)

    suspend fun insertarReunion(meetingEntity: MeetingEntity): Int {
        val idCreated = service.insertarReunion(meetingEntity)
        println("Id created: $idCreated")
        return idCreated.toInt()
    }

    suspend fun getMeetingFromCategoryId(categoryId:Int): List<MeetingEntity> {
        return service.getMeetingFromCategoryId(categoryId)
    }

    suspend fun deleteMeeting(meetingId: Int) {
        return service.deleteMeeting(meetingId)
    }
}