package com.example.appseguimientoacuerdos.Domain

import android.app.Application
import com.example.appseguimientoacuerdos.Data.Database.FollowAgreementsDB
import com.example.appseguimientoacuerdos.Data.Database.Entities.UserEntity
import com.example.appseguimientoacuerdos.Data.Network.SignupService

class SignupUseCase(application: Application) {
    private val daoLogin = FollowAgreementsDB.getDatabase(application.applicationContext).followAgreementsDao()
    private val service = SignupService(daoLogin)

    /**
     * Check if user is already registred
     * */
    suspend fun registrarUsuario(user:UserEntity):Boolean{
        val ban = if(!service.isUserRegistred(user)){
            service.registrarUsuario(user)
            true
        }else{
            false
        }
        return ban
    }
}