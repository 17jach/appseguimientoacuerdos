package com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Data.Repository.Response.AgreementRecyclerResponse
import com.example.appseguimientoacuerdos.R
import com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Fragments.MuestraDetallesAcuerdoFragment

class MuestraAcuerdosAdapter(
    val l: List<AgreementRecyclerResponse>,
    val fragmentManager: FragmentManager
) : RecyclerView.Adapter<MuestraAcuerdosAdapter.ViewHolder>() {
    val listAgreement = this.l

    class ViewHolder(val view: View, val fragmentManager: FragmentManager) :
        RecyclerView.ViewHolder(view) {

        lateinit var tvNombreAcuerdo: TextView
        lateinit var tvNombreReunion: TextView
        lateinit var tvPrioridadAcuerdo: TextView
        lateinit var tvDescripcionAcuerdo: TextView
        lateinit var tvPrioridadColor: TextView
        lateinit var tvFechaCumplimientoAcuerdo: TextView
        lateinit var tvStatusAcuerdo: TextView
        fun setPrioridadAgreement(priorityInput: Int): String {
            return when (priorityInput) {
                1 -> return "Critica"
                2 -> return "Importante"
                3 -> return "Normal"
                else -> {
                    return ""
                }
            }
        }

        //Jonathan branch
        fun setStatusAgreement(statusInput: Int): String {
            return when (statusInput) {
                1 -> return "Pendiente"
                2 -> return "En progreso"
                3 -> return "Terminado"
                else -> {
                    return ""
                }
            }
        }


        fun bind(agreementRecyclerResponse: AgreementRecyclerResponse) {
            tvNombreAcuerdo = view.findViewById(R.id.tvNombreAcuerdo)
            tvNombreReunion = view.findViewById(R.id.tvNombreReunion)
            tvPrioridadAcuerdo = view.findViewById(R.id.tvPrioridadAcuerdo)
            tvDescripcionAcuerdo = view.findViewById(R.id.tvDescripcionAcuerdo)
            tvFechaCumplimientoAcuerdo = view.findViewById(R.id.tvFechaCumplimientoAcuerdo)
            tvStatusAcuerdo = view.findViewById(R.id.tvStatusAcuerdo)
            tvPrioridadColor = view.findViewById(R.id.tvPrioridadAcuerdoColor)

            tvNombreAcuerdo.text = agreementRecyclerResponse.nameAgreement
            tvNombreReunion.text = agreementRecyclerResponse.titleMeeting
            tvPrioridadAcuerdo.text = setPrioridadAgreement(agreementRecyclerResponse.priority)
            tvDescripcionAcuerdo.text = agreementRecyclerResponse.description
            tvFechaCumplimientoAcuerdo.text = agreementRecyclerResponse.finishDate
            tvStatusAcuerdo.text = setStatusAgreement(agreementRecyclerResponse.status)

            /*Design color configuration*/

            setBackgroudColorPrioridad(tvPrioridadAcuerdo, tvPrioridadColor)


            val card: CardView = view.findViewById(R.id.cardContainerAgreement)
            card.setOnClickListener {
                Component.showToastMessage(
                    view.context,
                    "Click para actualizar - Funcionalidad no implementada"
                )
                //navigateToUpdateAgreement(agreementRecyclerResponse.idAgreement)
            }

        }

        @SuppressLint("ResourceAsColor")
        private fun setBackgroudColorPrioridad(
            tvPrioridadAcuerdo: TextView,
            tvPrioridadAcuerdoColor: TextView
        ) {
            val priorityInput = tvPrioridadAcuerdo.text.toString()
            when (priorityInput) {
                "Critica" -> tvPrioridadAcuerdoColor.apply { setBackgroundResource(R.color.red) }
                "Importante" -> tvPrioridadAcuerdoColor.apply { setBackgroundResource(R.color.yellow) }
                "Normal" -> tvPrioridadAcuerdoColor.apply { setBackgroundResource(R.color.green) }
            }

        }

        private fun navigateToUpdateAgreement(idAgreement: Int) {
            Component.replaceFragment(
                fragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                MuestraDetallesAcuerdoFragment(idAgreement)
            )
            println("GO TO SHOW DETAILS")

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_list_acuerdos, parent, false)
        return ViewHolder(view, fragmentManager)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listAgreement[position])
    }

    override fun getItemCount(): Int {
        return listAgreement.size
    }

    fun getAgreementId(position: Int): Int {
        val currentAgreement = this.listAgreement[position]
        return currentAgreement.idAgreement
    }
}