package com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.appseguimientoacuerdos.Data.Database.Entities.PriorityEntity
import com.example.appseguimientoacuerdos.R

class MuestraPrioridadSpinnerAdapter(
    val ctx:Context,
    val list:List<String>
    ):ArrayAdapter<String>(ctx,0,list) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, convertView, parent)
    }

    fun createItemView(position:Int, recycledView: View?, parent: ViewGroup): View {
        val priority = getItem(position)
        val view = recycledView?: LayoutInflater.from(parent.context).inflate(
            R.layout.item_spinner_list_priority,
            parent,
            false
        )

        //Si funciona tenemos que hacer otro item o factorizar para reutilizar
        //Hcer uno para cada uno
        view.findViewById<TextView>(R.id.tvPrioridadSpinner).text = priority
        return view
    }


    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, convertView, parent)
    }
}