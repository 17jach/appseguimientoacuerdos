package com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.appseguimientoacuerdos.R

class MuestraStatusSpinnerAdapter(
    val ctx:Context,
    val list:List<String>
    ):ArrayAdapter<String>(ctx, 0, list) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, convertView, parent)
    }

    fun createItemView(position:Int, recycledView: View?, parent: ViewGroup): View {
        val status = getItem(position)
        val view = recycledView?: LayoutInflater.from(parent.context).inflate(
            R.layout.item_spinner_list_status,
            parent,
            false
        )

        //Si funciona tenemos que hacer otro item o factorizar para reutilizar
        view.findViewById<TextView>(R.id.tvStatusSpinner).text = status
        return view
    }


    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, convertView, parent)
    }
}