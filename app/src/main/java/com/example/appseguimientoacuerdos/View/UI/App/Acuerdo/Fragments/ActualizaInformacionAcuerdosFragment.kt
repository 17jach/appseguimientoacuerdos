package com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.appseguimientoacuerdos.R
import com.example.appseguimientoacuerdos.databinding.FragmentActualizaInformacionAcuerdosBinding

class ActualizaInformacionAcuerdosFragment : Fragment() {

    private lateinit var _binding: FragmentActualizaInformacionAcuerdosBinding
    private val binding get() = _binding



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentActualizaInformacionAcuerdosBinding.inflate(inflater, container, false)
        return binding.root
    }
}