package com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments.MuestraReunionesFragment
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Acuerdos.AgregarAcuerdosViewModel
import com.example.appseguimientoacuerdos.databinding.FragmentAgregarAcuerdoP1Binding

class AgregarAcuerdoFragmentP1(val categoryId:Int, val idMeetingCurrent:Int) : Fragment() {

    var _binding: FragmentAgregarAcuerdoP1Binding? = null
    val binding get() = _binding!!
    lateinit var agregarAcuerdosViewModel:AgregarAcuerdosViewModel



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAgregarAcuerdoP1Binding.inflate(inflater, container, false)
        agregarAcuerdosViewModel = AgregarAcuerdosViewModel(requireActivity().application)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        /*Solo para test de nav*/
        val btnGoSecondScreen =binding.btnAgregarAcuerdoGoToSecondScreen
        val tituloAcuerdo = binding.textInputEditNombreAcuerdo
        val descripcionAcuerdo = binding.textInputEditDescripcionAcuerdo


        btnGoSecondScreen.setOnClickListener {
            val titulo:String = tituloAcuerdo.text.toString()
            val descripcion:String = descripcionAcuerdo.text.toString()

            agregarAcuerdosViewModel.setNumberAgreement(titulo)
            agregarAcuerdosViewModel.setDescriptionAgreement(descripcion)
            agregarAcuerdosViewModel.setIdMeetingOwner(idMeetingCurrent)

            Component.replaceFragment(
                parentFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                AgregarAcuerdoFragmentP3(
                    categoryId,
                    agregarAcuerdosViewModel
                )
            )
        }


        requireActivity().onBackPressedDispatcher.addCallback(this.viewLifecycleOwner, object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                Component.replaceFragment(
                    parentFragmentManager,
                    Component.REUNIONES_FRAGMENT_CONTAINER,
                    MuestraReunionesFragment(categoryId)
                )
            }
        })
    }

}