package com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Core.DatePickerHelper
import com.example.appseguimientoacuerdos.View.UI.App.Categorias.Fragments.MuestraCategoriasFragment
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments.MuestraReunionesFragment
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Acuerdos.AgregarAcuerdosViewModel
import com.example.appseguimientoacuerdos.databinding.FragmentAgregarAcuerdoP2Binding
import com.google.android.material.textfield.TextInputEditText
import java.util.*


class AgregarAcuerdoFragmentP2(
    val categoryId: Int,
    val agregarAcuerdosViewModel: AgregarAcuerdosViewModel
) :
    Fragment() {

    var _binding: FragmentAgregarAcuerdoP2Binding? = null
    val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAgregarAcuerdoP2Binding.inflate(layoutInflater, container, false)
        return binding.root
    }



    @SuppressLint("NewApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val idAgreementCreated = agregarAcuerdosViewModel.getIdCurrentAgreement()
        println("Trabajamos con el id: $idAgreementCreated")

        val txtInvolucrados = binding.textInputEditInvolucradosReunion

        val btnDefinirResponsable = binding.btnDefinirResponsable

        val btnGoToThirdScreen = binding.btnAgregarAcuerdoGoToThirdScreen

        val txtResponsables = binding.textInputEditResponsableAcuerdo


        agregarAcuerdosViewModel.responsableListMLD.observe(this.viewLifecycleOwner, Observer {
            var resp = ""
            for (r in it){
                resp+="${r.responsableName}. "
            }
            println("LISTA DE RESPONSABLES PARA MOSTRAR: $resp")
            txtResponsables.apply {
                setText(resp)
            }
        })

        agregarAcuerdosViewModel.involvedByMeetingList.observe(this.viewLifecycleOwner, Observer {
            var listOnText: String = ""
            for (involved in it) {
                listOnText += "${involved.involvedName}. "
            }
            txtInvolucrados.apply {
                setText(listOnText)
            }
        })

        agregarAcuerdosViewModel.getAllInvolvedByMeeting(
            agregarAcuerdosViewModel.getIdMeetingOwner()
        )




        btnDefinirResponsable.setOnClickListener {
            val currentIdMeeting = agregarAcuerdosViewModel.getIdMeetingOwner()
            val dialogInvolved = DialogFragmentResponsable(agregarAcuerdosViewModel.getIdCurrentAgreement(), agregarAcuerdosViewModel)
            dialogInvolved.show(parentFragmentManager, "Dialog Involved")
        }




        btnGoToThirdScreen.setOnClickListener {
//Finished click
            //agregarAcuerdosViewModel.updateAcuerdo(currentIdAcuerdo)
            Component.replaceFragment(
                parentFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                MuestraAcuerdosFragment(categoryId, agregarAcuerdosViewModel.getIdMeetingOwner())
            )
        }


    }

}