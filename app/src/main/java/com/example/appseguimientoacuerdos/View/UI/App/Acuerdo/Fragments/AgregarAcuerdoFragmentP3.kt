package com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Core.DatePickerHelper
import com.example.appseguimientoacuerdos.Data.Database.Entities.AgreementEntity
import com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Adapters.MuestraPrioridadSpinnerAdapter
import com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Adapters.MuestraStatusSpinnerAdapter
import com.example.appseguimientoacuerdos.View.UI.App.Categorias.Fragments.MuestraCategoriasFragment
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments.MuestraReunionesFragment
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments.SinReunionesFragment
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Acuerdos.AgregarAcuerdosViewModel
import com.example.appseguimientoacuerdos.databinding.FragmentAgregarAcuerdoP3Binding
import com.google.android.material.textfield.TextInputEditText
import java.util.*

class AgregarAcuerdoFragmentP3(
    val categoryId: Int,
    val agregarAcuerdosViewModel: AgregarAcuerdosViewModel
) :
    Fragment() {

    var _binding: FragmentAgregarAcuerdoP3Binding? = null
    val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAgregarAcuerdoP3Binding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*
        * Calendar
        * */

        val calendarPicker = binding.textInputLayoutDatePicker
        var dateFinishFromFragment: String = getTodayDate()


        setTodayDateOnDatePicker(calendarPicker)

        //Dentro del click
        //            agregarAcuerdosViewModel.setFinishDateFromFragment(dateFinishFromFragment)


        val spinnerPrioridad = binding.autoCompletePrioridadSpinner
        val spinnerStatus = binding.autoCompleteStatusSpinner
        var statusListData = listOf<String>()
        var priorityListData = listOf<String>()
        val btnAgregarAcuerdo = binding.btnAgregarAcuerdoGoToMostrarAcuerdos

        agregarAcuerdosViewModel.getAllPriorityData()
        agregarAcuerdosViewModel.getAllStatusData()


        agregarAcuerdosViewModel.priorityDataList.observe(this.viewLifecycleOwner, Observer {
            priorityListData = it
            spinnerPrioridad.apply {
                setAdapter(MuestraPrioridadSpinnerAdapter(requireContext(), priorityListData))
            }
        })
        agregarAcuerdosViewModel.statusDataList.observe(this.viewLifecycleOwner, Observer {
            statusListData = it
            spinnerStatus.apply {
                setAdapter(MuestraStatusSpinnerAdapter(requireContext(), statusListData))
            }
        })


        with(spinnerStatus) {
            setAdapter(MuestraStatusSpinnerAdapter(requireContext(), statusListData))
            onItemClickListener = AdapterView.OnItemClickListener(
                fun(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val statsS = parent?.getItemAtPosition(position) as String
                    println("Status on itemClick: $statsS")
                    agregarAcuerdosViewModel.setStatusAgreement(statsS)
                }
            )
        }

        with(spinnerPrioridad) {
            setAdapter(MuestraPrioridadSpinnerAdapter(requireContext(), priorityListData))
            onItemClickListener = AdapterView.OnItemClickListener(
                fun(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val prioridadS = parent?.getItemAtPosition(position) as String
                    println("Prioridad on itemClick: $prioridadS")
                    agregarAcuerdosViewModel.setPrioridadAgreement(prioridadS)
                }
            )
        }

        btnAgregarAcuerdo.setOnClickListener {

            val currentAgreement = AgreementEntity(
                0,
                agregarAcuerdosViewModel.getNumberAgreement(),
                agregarAcuerdosViewModel.getDescription(),
                agregarAcuerdosViewModel.getCreateDate(),
                agregarAcuerdosViewModel.getFinishDate(),
                agregarAcuerdosViewModel.getIdMeetingOwner(),
                agregarAcuerdosViewModel.getPrioridad(),
                agregarAcuerdosViewModel.getStatus()
            )

            agregarAcuerdosViewModel.insertAcuerdo(currentAgreement)


            Component.replaceFragment(
                 parentFragmentManager,
                 Component.REUNIONES_FRAGMENT_CONTAINER,
                 AgregarAcuerdoFragmentP2(
                     categoryId,
                     agregarAcuerdosViewModel
                 )
             )


        }



        agregarAcuerdosViewModel.finishDateFromDatePicker.observe(
            this.viewLifecycleOwner,
            Observer {
                dateFinishFromFragment = it
                calendarPicker.apply {
                    setText(dateFinishFromFragment)
                    agregarAcuerdosViewModel.setFinishDateFromFragment(dateFinishFromFragment)
                }
            })
        calendarPicker.setOnClickListener {
            try {
                showDatePicker()
            } catch (e: Exception) {
                println("Error: ${e.message}")
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this.viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    Component.replaceFragment(
                        parentFragmentManager,
                        Component.REUNIONES_FRAGMENT_CONTAINER,
                        MuestraReunionesFragment(categoryId)
                    )
                }
            }
        )

    }


    private fun setTodayDateOnDatePicker(calendarPicker: TextInputEditText) {

        val date = getTodayDate()
        agregarAcuerdosViewModel.setCreateDate(date)
        agregarAcuerdosViewModel.setFinishDateFromFragment(date)
        calendarPicker.apply {
            setText(date)
        }
    }

    fun getTodayDate(): String {
        val c = Calendar.getInstance()
        val day = c.get(Calendar.DAY_OF_MONTH)
        val month = c.get(Calendar.MONTH)
        val year = c.get(Calendar.YEAR)

        val correctMonth = getCorrectMonth(month)

        return "$day/$correctMonth/$year"
    }

    private fun showDatePicker() {
        val datePicker = DatePickerHelper { day, month, year ->
            onDateSelected(day, month, year)
        }
        datePicker.show(parentFragmentManager, "DatePicker")
    }

    private fun onDateSelected(day: Int, month: Int, year: Int) {
        val monthCorrect = getCorrectMonth(month)
        val dateInText = "$day/$monthCorrect/$year"
        agregarAcuerdosViewModel.setFinishDateFromDatePicker(dateInText)
    }

    private fun getCorrectMonth(month: Int): String {
        return when (month) {
            0 -> return "Enero"
            1 -> return "Febrero"
            2 -> return "Marzo"
            3 -> return "Abril"
            4 -> return "Mayo"
            5 -> return "Junio"
            6 -> return "Julio"
            7 -> return "Agosto"
            8 -> return "Septiembre"
            9 -> return "Octubre"
            10 -> return "Noviembre"
            11 -> return "Diciembre"
            else -> {
                return ""
            }
        }
    }

}