package com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Data.Database.Entities.ResponsableEntity
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Acuerdos.AgregarAcuerdosViewModel
import com.example.appseguimientoacuerdos.databinding.DialogNuevoInvolucradoBinding

class DialogFragmentResponsable(
    val idAgreementOwner: Int,
    val agregarAcuerdosViewModel: AgregarAcuerdosViewModel
) : DialogFragment() {
    private lateinit var _binding: DialogNuevoInvolucradoBinding
    private val binding get() = _binding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogNuevoInvolucradoBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val spinnerResponsables = binding.autoCompleteResponsableAcuerdo
        val listaTest = arrayListOf<String>()
        lateinit var responsableSelected: ResponsableEntity

        spinnerResponsables.apply {
            setAdapter(
                ArrayAdapter<String>(
                    requireContext(),
                    androidx.constraintlayout.widget.R.layout.support_simple_spinner_dropdown_item,
                    listaTest
                )
            )
        }

        agregarAcuerdosViewModel.getAllInvolvedByMeeting(
            agregarAcuerdosViewModel.getIdMeetingOwner()
        )


        agregarAcuerdosViewModel.involvedByMeetingList.observe(this.viewLifecycleOwner, Observer {

            val involvedNameList = arrayListOf<String>()
            for (involved in it) {
                involvedNameList.add(involved.involvedName)
            }


            spinnerResponsables.apply {
                setAdapter(
                    ArrayAdapter<String>(
                        requireContext(),
                        androidx.constraintlayout.widget.R.layout.support_simple_spinner_dropdown_item,
                        involvedNameList
                    )
                )
                onItemClickListener = AdapterView.OnItemClickListener(
                    fun(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        val name = parent?.getItemAtPosition(position) as String
                        val responsable = ResponsableEntity(
                            0,
                            name,
                            idAgreementOwner
                        )

                        responsableSelected = responsable
                        Component.showToastMessage(requireContext(), "Responsable seleccionado")
                    }
                )
            }
        })

        val btnAgregar: Button = binding.btnAgregarInvolucradoDialog
/*
        agregarAcuerdosViewModel.closeDialogMLD.observe(this.viewLifecycleOwner, Observer {
            if(it){
                println("Cerrar")
                dismiss()
            }else{
                println("Mantener abierto")
            }
        })

 */

        btnAgregar.setOnClickListener {

            if(responsableSelected!=null){
                agregarAcuerdosViewModel.addResponsable(responsableSelected)
                //agregarAcuerdosViewModel.getResponsables(idAgreementOwner)
                dismiss()
            }else{
                Component.showToastMessage(requireContext(), "Selecciona un responsable")
            }


        }
    }
}