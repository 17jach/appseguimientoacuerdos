package com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Data.Database.Entities.ResponsableEntity
import com.example.appseguimientoacuerdos.Data.Repository.Response.AgreementRecyclerResponse
import com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Adapters.MuestraAcuerdosAdapter
import com.example.appseguimientoacuerdos.View.UI.App.Categorias.Adapters.MuestraCategoriasAdapter
import com.example.appseguimientoacuerdos.View.UI.App.Categorias.Fragments.MuestraCategoriasFragment
import com.example.appseguimientoacuerdos.View.UI.App.Involucrados.Adapters.SwipeDeleteMeetingCallback
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments.MuestraReunionesFragment
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Acuerdos.MuestraAcuerdosViewModel
import com.example.appseguimientoacuerdos.databinding.FragmentMuestraAcuerdosBinding

class MuestraAcuerdosFragment(val categoryId: Int, val meetingId: Int) : Fragment() {


    var _binding: FragmentMuestraAcuerdosBinding? = null
    val binding get() = _binding!!

    lateinit var adapterMuestraAcuerdos: MuestraAcuerdosAdapter
    lateinit var muestraAcuerdosViewModel: MuestraAcuerdosViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMuestraAcuerdosBinding.inflate(layoutInflater, container, false)
        muestraAcuerdosViewModel = MuestraAcuerdosViewModel(requireActivity().application)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val rvAcuerdos = binding.rvContainerAcuerdos
        var listaAcuerdos = listOf<AgreementRecyclerResponse>()
        val btnNuevoAcuerdo = binding.btnGoToNuevoAcuerdo
        val spinnerFiltro = binding.autoCompleteFiltroAcuerdosSpinner
        val btnDownloadReport = binding.btnDownloadReport

        val listFiltros = arrayListOf<String>()

        listFiltros.add(Component.FILTRO_TODOS_LOS_ACUERDOS_TEXTO)
        listFiltros.add(Component.FILTRO_EN_PROCESO_TEXTO)
        listFiltros.add(Component.FILTRO_PENDIENTES_TEXTO)
        listFiltros.add(Component.FILTRO_TERMINADOS_TEXTO)
        listFiltros.add(Component.FILTRO_PRIORIDAD_TEXTO)

        muestraAcuerdosViewModel.setFiltro(Component.FILTRO_TODOS_LOS_ACUERDOS_TEXTO)


        muestraAcuerdosViewModel.filtroMLD.observe(this.viewLifecycleOwner, Observer {
            println("Filtrando por: $it")
            muestraAcuerdosViewModel.getAllAcuerdosResponse(meetingId)
            //SQL ORDER BY idPriority  DESC or Empty
        })



        spinnerFiltro.apply {
            setAdapter(
                ArrayAdapter<String>(
                    requireContext(),
                    androidx.constraintlayout.widget.R.layout.support_simple_spinner_dropdown_item,
                    listFiltros
                )
            )
            onItemClickListener = AdapterView.OnItemClickListener(
                fun(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val filtro = parent?.getItemAtPosition(position) as String
                    muestraAcuerdosViewModel.setFiltro(filtro)
                }
            )
        }


        adapterMuestraAcuerdos = MuestraAcuerdosAdapter(listaAcuerdos, parentFragmentManager)
        //muestraAcuerdosViewModel.getAllAcuerdosResponse(meetingId)

        rvAcuerdos.apply {
            adapter = adapterMuestraAcuerdos
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }

        muestraAcuerdosViewModel.isAcuerdosListEmpty.observe(
            this.viewLifecycleOwner,
            Observer { isAgreementListEmpty ->
                if (isAgreementListEmpty) {
                    renderEmptyView()
                } else {
                    muestraAcuerdosViewModel.acuerdosResponseList.observe(
                        this.viewLifecycleOwner,
                        Observer {
                            listaAcuerdos = it
                            adapterMuestraAcuerdos =
                                MuestraAcuerdosAdapter(listaAcuerdos, parentFragmentManager)
                            rvAcuerdos.apply {
                                adapter = adapterMuestraAcuerdos
                            }
                        })
                }
            })

        btnNuevoAcuerdo.setOnClickListener {
            Component.replaceFragment(
                parentFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                AgregarAcuerdoFragmentP1(categoryId, meetingId)
            )
        }

        btnDownloadReport.setOnClickListener {
            Component.showToastMessage(requireContext(), "Error downloading report")

        }



        val swipeDeleteMeetingCallback = object : SwipeDeleteMeetingCallback() {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                println("Position que regresa el viewHolder: $position")
                //adapter.deleteInvolved(position)
                //adapter.notifyItemRemoved(position)
                println(
                    "Id que regresa con el adapter.getInvolvedId: ${
                        adapterMuestraAcuerdos.getAgreementId(
                            position
                        )
                    }"
                )
                muestraAcuerdosViewModel.deleteAgreement(
                    adapterMuestraAcuerdos.getAgreementId(
                        position
                    )
                )

                muestraAcuerdosViewModel.getAllAcuerdosResponse(meetingId)
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeDeleteMeetingCallback)

        itemTouchHelper.attachToRecyclerView(rvAcuerdos)

        requireActivity().onBackPressedDispatcher.addCallback(
            this.viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    Component.replaceFragment(
                        parentFragmentManager,
                        Component.REUNIONES_FRAGMENT_CONTAINER,
                        MuestraReunionesFragment(categoryId)
                    )
                }
            })

    }


    private fun renderEmptyView() {
        Component.replaceFragment(
            parentFragmentManager,
            Component.REUNIONES_FRAGMENT_CONTAINER,
            SinAcuerdosFragment(categoryId, meetingId)
        )
    }


}