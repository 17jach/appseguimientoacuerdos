package com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.R
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Acuerdos.MuestraDetallesAcuerdoViewModel
import com.example.appseguimientoacuerdos.databinding.FragmentMuestraInformacionAcuerdosBinding as FragmentMuestraDetallesAcuerdoBinding


class MuestraDetallesAcuerdoFragment(val idAgreement: Int) : Fragment() {
    private lateinit var _binding: FragmentMuestraDetallesAcuerdoBinding
    private val binding get() = _binding
    private lateinit var muestraDetallesAcuerdoViewModel: MuestraDetallesAcuerdoViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMuestraDetallesAcuerdoBinding.inflate(inflater, container, false)
        muestraDetallesAcuerdoViewModel =
            MuestraDetallesAcuerdoViewModel(requireActivity().application)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        muestraDetallesAcuerdoViewModel.getAgreementById(idAgreement)
        muestraDetallesAcuerdoViewModel.infoAgreementRespone.observe(
            this.viewLifecycleOwner,
            Observer { agreementEntity ->
                println("Item:  $agreementEntity")
                binding.textInputEditNombreAcuerdoSDA.apply {
                    setText(agreementEntity.numberAgreement)
                }
                binding.textInputEditDescripcionAcuerdoSDA.apply {
                    setText(agreementEntity.description)
                }
                binding.textInputEditFechaAcuerdoCreacionSDA.apply {
                    setText(agreementEntity.createDate)
                }
                binding.textInputEditFechaAcuerdoFinalizacionSDA.apply {
                    setText(agreementEntity.finishDate)
                }

                binding.textInputEditResponableAcuerdoSDA.apply {
                    setText(agreementEntity.numberAgreement)
                }
                binding.autoCompletePrioridadSpinner.apply {
                    setText(getPrioridadAgreement(agreementEntity.priority))
                }

                binding.autoCompleteStatusSpinner.apply {
                    setText(getPrioridadAgreement(agreementEntity.status))
                }
            })

        muestraDetallesAcuerdoViewModel.priorityDataList.observe(this.viewLifecycleOwner, Observer {

            binding.autoCompletePrioridadSpinner.apply {
                setAdapter(
                    ArrayAdapter<String>(
                        requireContext(),
                        R.layout.item_spinner_list_priority,
                        it
                    )
                )
                onItemClickListener = AdapterView.OnItemClickListener(
                    fun(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        val prioridadS = parent?.getItemAtPosition(position) as String
                        println("Prioridad on itemClick: $prioridadS")

                    }
                )

            }

        })

        muestraDetallesAcuerdoViewModel.getAllPriorityData()
        muestraDetallesAcuerdoViewModel.getAllStatusData()


    }

    fun getPrioridadAgreement(prioridad: Int): String {
        var prioridadText = ""
        when (prioridad) {
            Component.PRIORIDAD_CRITICA -> prioridadText = "Critica"
            Component.PRIORIDAD_IMPORTANTE -> prioridadText = "Importante"
            Component.PRIORIDAD_NORMAL -> prioridadText = "Normal"
        }
        return prioridadText
    }

    fun getStatusAgreement(status: Int): String {
        var statusText = ""
        when (status) {
            Component.STATUS_PENDIENTE -> statusText = "Pendiente"
            Component.STATUS_EN_PROGRESO -> statusText = "En progreso"
            Component.STATUS_TERMINADO -> statusText = "Terminado"
        }
        return statusText
    }


}