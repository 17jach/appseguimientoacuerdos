package com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.View.UI.App.Categorias.Fragments.MuestraCategoriasFragment
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments.MuestraReunionesFragment
import com.example.appseguimientoacuerdos.databinding.FragmentSinAcuerdosBinding

class SinAcuerdosFragment(val categoryId:Int, val meetingId: Int) : Fragment() {

    private lateinit var _binding: FragmentSinAcuerdosBinding
    private val binding get() = _binding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSinAcuerdosBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnNuevoAcuerdo.setOnClickListener {
            Component.replaceFragment(
                parentFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                AgregarAcuerdoFragmentP1(categoryId, meetingId)
            )
        }


        requireActivity().onBackPressedDispatcher.addCallback(
            this.viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    Component.replaceFragment(
                        parentFragmentManager,
                        Component.REUNIONES_FRAGMENT_CONTAINER,
                        MuestraReunionesFragment(categoryId)
                    )
                }
            })
    }
}