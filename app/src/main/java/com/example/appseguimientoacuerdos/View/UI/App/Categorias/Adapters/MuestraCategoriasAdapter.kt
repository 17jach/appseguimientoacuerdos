package com.example.appseguimientoacuerdos.View.UI.App.Categorias.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Data.Database.Entities.CategoryEntity
import com.example.appseguimientoacuerdos.R
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments.MuestraReunionesFragment
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments.NuevaReunionFragment

//Se supone que utilice CategryRecyclerResponse pero creo que funciona con CategoryENTITY
class MuestraCategoriasAdapter(
    val l:List<CategoryEntity>,
    val fragmentManager: FragmentManager
    ):RecyclerView.Adapter<MuestraCategoriasAdapter.ViewHolder>() {
    val listCategoryEntity = this.l

    class ViewHolder(val view: View, val fragmentManager:FragmentManager):RecyclerView.ViewHolder(view) {
        lateinit var tvTitleCategory:TextView
        lateinit var btnNuevaReunionDeCategoria:TextView
        lateinit var btnMostrarReunionesDeCategoria:TextView


        fun bind(categoryEntity: CategoryEntity){
            tvTitleCategory = view.findViewById(R.id.tvTitleCategory)
            btnNuevaReunionDeCategoria = view.findViewById(R.id.btnAgregarReunionDeCategoria)
            btnMostrarReunionesDeCategoria = view.findViewById(R.id.btnListarReunionesDeCategoria)

            tvTitleCategory.text = categoryEntity.title
            btnNuevaReunionDeCategoria.setOnClickListener {
                Component.replaceFragment(
                    fragmentManager,
                    Component.REUNIONES_FRAGMENT_CONTAINER,
                    NuevaReunionFragment(categoryEntity.categoryId, Component.RETURN_CATEGORY_ON_BACK_PRESSED)
                )
            }

            btnMostrarReunionesDeCategoria.setOnClickListener {
                Component.replaceFragment(
                    fragmentManager,
                    Component.REUNIONES_FRAGMENT_CONTAINER,
                    MuestraReunionesFragment(categoryEntity.categoryId)
                )
            }

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_categorias, parent, false)
        return ViewHolder(view, fragmentManager)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listCategoryEntity[position])
    }

    override fun getItemCount(): Int {
        return listCategoryEntity.size
    }
    fun getCategoryId(position: Int):Int{
        val currentCategory = listCategoryEntity[position]
        return currentCategory.categoryId

    }

}