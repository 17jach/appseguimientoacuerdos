package com.example.appseguimientoacuerdos.View.UI.App.Categorias.Fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Core.Prefs
import com.example.appseguimientoacuerdos.R
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Categorias.AgregarCategoriasViewModel
import com.example.appseguimientoacuerdos.databinding.FragmentAgregarCategoriaBinding

class AgregarCategoriaFragment : Fragment() {

    private lateinit var _binding: FragmentAgregarCategoriaBinding
    private val binding get() = _binding
    private lateinit var agregarCategoriaViewModel:AgregarCategoriasViewModel
    private lateinit var prefs:Prefs

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAgregarCategoriaBinding.inflate(layoutInflater, container, false)
        agregarCategoriaViewModel = AgregarCategoriasViewModel(requireActivity().application)
        prefs = Prefs(requireContext())
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textInputEditTituloCategoria.requestFocus()




        val btnAgregarCategoria = binding.btnAgregarCategoria


        btnAgregarCategoria.setOnClickListener {
            val tituloCategoria = binding.textInputEditTituloCategoria.text.toString()
            val idUserOwner = prefs.getUserId()
            agregarCategoriaViewModel.agregarCategoria(tituloCategoria, idUserOwner)
            Component.replaceFragment(
                parentFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                MuestraCategoriasFragment()
            )
        }
    }
}