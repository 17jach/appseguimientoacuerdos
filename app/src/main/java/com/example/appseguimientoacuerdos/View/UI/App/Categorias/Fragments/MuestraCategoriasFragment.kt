package com.example.appseguimientoacuerdos.View.UI.App.Categorias.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Core.Prefs
import com.example.appseguimientoacuerdos.Data.Database.Entities.CategoryEntity
import com.example.appseguimientoacuerdos.View.UI.App.Categorias.Adapters.MuestraCategoriasAdapter
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Adapters.SwipeDeleteMeetingCallback
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Categorias.MuestraCategoriasViewModel
import com.example.appseguimientoacuerdos.databinding.FragmentMostrarCategoriasBinding

class MuestraCategoriasFragment : Fragment() {
    private lateinit var _binding: FragmentMostrarCategoriasBinding
    private val binding get() = _binding
    private lateinit var muestraCategoriasViewModel: MuestraCategoriasViewModel
    private lateinit var prefs: Prefs
    private var pressedTime: Long = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMostrarCategoriasBinding.inflate(layoutInflater, container, false)
        muestraCategoriasViewModel = MuestraCategoriasViewModel(requireActivity().application)
        prefs = Prefs(requireContext())
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val categoryList = arrayListOf<CategoryEntity>()
        var muestraCategoriasAdapter = MuestraCategoriasAdapter(categoryList, parentFragmentManager)
        muestraCategoriasViewModel.getCategoryList(prefs.getUserId())

        val btnGoToNuevaCategoria = binding.btnGoToNuevaCategoria

        btnGoToNuevaCategoria.setOnClickListener {
            Component.replaceFragment(
                parentFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                AgregarCategoriaFragment()
            )
        }

        val rvCategoriasList = binding.rvContainerCategoria

        muestraCategoriasViewModel.categorysList.observe(
            viewLifecycleOwner,
            Observer { categoryList ->
                if (categoryList.isEmpty()) {
                    Component.replaceFragment(
                        parentFragmentManager,
                        Component.REUNIONES_FRAGMENT_CONTAINER,
                        SinCategoriasFragment()
                    )
                }else{
                    muestraCategoriasAdapter = MuestraCategoriasAdapter(categoryList, parentFragmentManager)
                    rvCategoriasList.adapter = muestraCategoriasAdapter
                    rvCategoriasList.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                }
            })

        val swipeDeleteMeetingCallback = object :SwipeDeleteMeetingCallback(){
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                muestraCategoriasViewModel.deleteMeeting(muestraCategoriasAdapter.getCategoryId(position))
                muestraCategoriasViewModel.getCategoryList(prefs.getUserId())

            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeDeleteMeetingCallback)
        itemTouchHelper.attachToRecyclerView(rvCategoriasList)

        requireActivity().onBackPressedDispatcher.addCallback(
            this.viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (pressedTime + 2000 > System.currentTimeMillis()) {
                        requireActivity().finish();
                    } else {
                        Component.showToastMessage(requireContext(), "Presiona otra vez para salir")
                    }
                    pressedTime = System.currentTimeMillis();
                }
            })


    }


}
