package com.example.appseguimientoacuerdos.View.UI.App.Categorias.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.R
import com.example.appseguimientoacuerdos.databinding.FragmentSinCategoriasBinding

class SinCategoriasFragment : Fragment() {

    private lateinit var _binding: FragmentSinCategoriasBinding
    private val binding get() = _binding
    private var pressedTime: Long = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSinCategoriasBinding.inflate(layoutInflater, container, false)



        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val btnNewCategory = binding.btnNuevaCategoria
        btnNewCategory.setOnClickListener {
            Component.replaceFragment(
                parentFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                AgregarCategoriaFragment()
            )
        }

        requireActivity().onBackPressedDispatcher.addCallback(
            this.viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (pressedTime + 2000 > System.currentTimeMillis()) {
                        requireActivity().finish();
                    } else {
                        Component.showToastMessage(requireContext(), "Presiona otra vez para salir")
                    }
                    pressedTime = System.currentTimeMillis();
                }
            })


    }


}