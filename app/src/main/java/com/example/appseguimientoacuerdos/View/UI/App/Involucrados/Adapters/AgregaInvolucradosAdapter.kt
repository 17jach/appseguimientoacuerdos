package com.example.appseguimientoacuerdos.View.UI.App.Involucrados.Adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appseguimientoacuerdos.Data.Database.Entities.InvolvedEntity
import com.example.appseguimientoacuerdos.R

class AgregaInvolucradosAdapter(
    val l:List<InvolvedEntity>,
    val layout:Int
): RecyclerView.Adapter<AgregaInvolucradosAdapter.ViewHolder>() {

    var list:MutableList<InvolvedEntity> = this.l as MutableList<InvolvedEntity>

    class ViewHolder(val view: View):RecyclerView.ViewHolder(view) {
        lateinit var tvInvolved:TextView
        lateinit var currentInvolved:InvolvedEntity
        fun bind(involvedEntity: InvolvedEntity, index: Int){
            tvInvolved = view.findViewById(R.id.tvInvolucrado)
            currentInvolved = involvedEntity
            tvInvolved.text = "Name: ${currentInvolved.involvedName} Meeting: ${currentInvolved.idMeetingOwnerInvolved}"

        }

    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position], position)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(listUpdated:List<InvolvedEntity>){
        this.list = listUpdated as MutableList<InvolvedEntity>
    }

    @SuppressLint("NotifyDataSetChanged")
    fun deleteInvolved(index:Int){
        println("El object que desilza es del index $index y pertence al involved ${this.list[index]}")

        this.list.removeAt(index)

        notifyDataSetChanged()
    }

    fun getInvolvedId(position: Int): Int {
        val currentInvolved = list[position]
        println("la función dentro del adapter recibe la position $position y el objeto que regresa es $currentInvolved")
        return currentInvolved.idInvolved
    }
}