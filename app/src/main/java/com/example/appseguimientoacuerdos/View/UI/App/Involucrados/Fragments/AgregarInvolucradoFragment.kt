package com.example.appseguimientoacuerdos.View.UI.App.Involucrados.Fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Data.Database.Entities.InvolvedEntity
import com.example.appseguimientoacuerdos.R
import com.example.appseguimientoacuerdos.View.UI.App.Involucrados.Adapters.AgregaInvolucradosAdapter
import com.example.appseguimientoacuerdos.View.UI.App.Involucrados.Adapters.SwipeDeleteMeetingCallback
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments.MuestraReunionesFragment
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Involucrados.AgregarInvolucradosViewModel
import com.example.appseguimientoacuerdos.databinding.FragmentAgregarInvolucradoBinding


class AgregarInvolucradoFragment(
    val idMeetingOwner: Int,
    val idCategoryOwner:Int
) : DialogFragment() {
    private var _binding: FragmentAgregarInvolucradoBinding? = null
    private val binding get() = _binding!!

    private lateinit var agregarInvolucradosViewModel: AgregarInvolucradosViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAgregarInvolucradoBinding.inflate(inflater, container, false)
        //instancia del viewmodel
        agregarInvolucradosViewModel = AgregarInvolucradosViewModel(requireActivity().application)
        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textInputEditNameInvolucrado.requestFocus()

        val btnFinalizar = binding.btnFinalizar
        val btnAgregarInvolved = binding.btnAgregarInvolucrados
        val involvedListEmpty = arrayListOf<InvolvedEntity>()

        var adapter = AgregaInvolucradosAdapter(involvedListEmpty, R.layout.item_list_involucrados)
        val rv = binding.rvInvolucradosListas



        rv.apply {
            setAdapter(adapter)
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }

        agregarInvolucradosViewModel.getAllInvolvedByMeeting(idMeetingOwner)

        agregarInvolucradosViewModel.listAllInvolved.observe(this.viewLifecycleOwner, Observer {
            adapter = AgregaInvolucradosAdapter(it, R.layout.item_list_involucrados)
            adapter.notifyDataSetChanged()
            rv.adapter = adapter


        })

        btnAgregarInvolved.setOnClickListener {
            val nuevoInvolved = InvolvedEntity(
                0,
                binding.textInputEditNameInvolucrado.text.toString(),
                binding.textInputEditPhoneInvolucrado.text.toString(),
                idMeetingOwner
            )
            //Funcion de limpiar
            binding.textInputEditNameInvolucrado.apply {
              setText("")
            }
            binding.textInputEditPhoneInvolucrado.apply {
                setText("")
            }
            agregarInvolucradosViewModel.insertInvolved(nuevoInvolved)
            agregarInvolucradosViewModel.getAllInvolvedByMeeting(idMeetingOwner)
            Component.replaceFragment(
                parentFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                AgregarInvolucradoFragment(idMeetingOwner, idCategoryOwner)
            )

        }

        btnFinalizar.setOnClickListener {
            Component.replaceFragment(
                parentFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                MuestraReunionesFragment(idCategoryOwner)
            )
        }

        val swipeDeleteInvolvedCallback = object : SwipeDeleteMeetingCallback(){
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                agregarInvolucradosViewModel.deleteInvolved(adapter.getInvolvedId(position))
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeDeleteInvolvedCallback)

        itemTouchHelper.attachToRecyclerView(rv)

        /*

        var listInvolved = arrayListOf<InvolvedEntity>()
        val adapter =
            AgregaInvolucradosAdapter(listInvolved, R.layout.item_list_involucrados)
        val recyclerInvolveds = binding.rvInvolucradosListas

        val btnAddInvolved = binding.btnAgregarInvolucrados

        //agregarInvolucradosViewModel.getAllInvolvedByAgreement(idAgreementOwner)


        btnAddInvolved.setOnClickListener {
            /*
            val name = binding.textInputEditInvolucrado.text.toString()
            val newInvolved = InvolvedEntity(0,name,"","", 2, idMeetingOwner)
            agregarInvolucradosViewModel.insertInvolved(newInvolved)
            agregarInvolucradosViewModel.getAllInvolvedByAgreement(idAgreementOwner)


             */
        }


        agregarInvolucradosViewModel.listAllInvolved.observe(this.viewLifecycleOwner, Observer {
            adapter.updateList(it)
            adapter.notifyDataSetChanged()
        })

        recyclerInvolveds.adapter = adapter
        recyclerInvolveds.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)


        val swipeDeleteInvolvedCallback = object : SwipeDeleteMeetingCallback(){
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                println("Position que regresa el viewHolder: $position")
                //adapter.deleteInvolved(position)
                //adapter.notifyItemRemoved(position)
                println("Id que regresa con el adapter.getInvolvedId: ${adapter.getInvolvedId(position)}")
                agregarInvolucradosViewModel.deleteInvolved(adapter.getInvolvedId(position))

                //agregarInvolucradosViewModel.getAllInvolvedByAgreement(idAgreementOwner)
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeDeleteInvolvedCallback)

        itemTouchHelper.attachToRecyclerView(recyclerInvolveds)


         */
    }

}