package com.example.appseguimientoacuerdos.View.UI.App.MyInfo.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Informacion.MostrarInfoViewModel
import com.example.appseguimientoacuerdos.databinding.DialogActualizarInformacionUsuarioBinding

class DialogFragmentInformacion(
    val userId: Int,
    val mostrarInfoViewModel: MostrarInfoViewModel
) : DialogFragment() {

    var _binding: DialogActualizarInformacionUsuarioBinding? = null
    val binding get() = _binding!!
    var typeSrc = 0

    //private lateinit var mostrarInfoViewModel: MostrarInfoViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding =
            DialogActualizarInformacionUsuarioBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val txtInput = binding.textInputEditUsername
        val btnActualizarInformacion = binding.btnActualizarInformacionUsuarioDialog
        val currentInfo: String = getCurrentInfoBySrc(getTypSrc())

        txtInput.apply {
            setText(currentInfo)
        }

        btnActualizarInformacion.setOnClickListener {

            updateInfoByTyp(getTypSrc(),txtInput.text.toString(), userId)


            Component.replaceFragment(
                parentFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                MyInformacion()
            )
            this.dismiss()

        }


    }

    private fun updateInfoByTyp(typSrc: Int, txtInput: String, userId: Int) {
        mostrarInfoViewModel.updateInfoByTyp(typSrc, txtInput, userId)
    }


    private fun getCurrentInfoBySrc(src:Int): String {
        var currentInfo = ""
        when (src) {
            1 -> currentInfo = mostrarInfoViewModel.getUsernameLiveData()
            2 -> currentInfo = mostrarInfoViewModel.getNumberLiveData()
            3 -> currentInfo = mostrarInfoViewModel.getPasswordLiveData()
        }
        return currentInfo
    }


    fun setTypSrc(type: Int) {
        this.typeSrc = type
    }
    fun getTypSrc(): Int {
        return this.typeSrc
    }
}