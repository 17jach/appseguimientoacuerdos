package com.example.appseguimientoacuerdos.View.UI.App.MyInfo.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Core.Prefs
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Informacion.MostrarInfoViewModel
import com.example.appseguimientoacuerdos.databinding.FragmentMuestraMyInformacionBinding
import com.google.android.material.textfield.TextInputEditText


class MyInformacion : Fragment() {
    private lateinit var _binding: FragmentMuestraMyInformacionBinding
    private val binding get() = _binding
    private lateinit var mostrarInfoViewModel: MostrarInfoViewModel
    private lateinit var prefs: Prefs
    private lateinit var etUserName: TextInputEditText
    private lateinit var etNumber: TextInputEditText
    private lateinit var etPassword: TextInputEditText

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMuestraMyInformacionBinding.inflate(layoutInflater, container, false)
        mostrarInfoViewModel = MostrarInfoViewModel(requireActivity().application)
        prefs = Prefs(requireContext())

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        etUserName = binding.textInputEditUsername
        etNumber = binding.textInputEditNumber
        etPassword = binding.textInputEditPassword
        mostrarInformacionViewModel()
        val dialogo = DialogFragmentInformacion(
            prefs.getUserId(),
            mostrarInfoViewModel
        )

        etUserName.setOnClickListener {
            dialogo.setTypSrc(1)
            dialogo.show(parentFragmentManager, "Update Dialog")
        }
        etNumber.setOnClickListener {
            dialogo.setTypSrc(2)
            dialogo.show(parentFragmentManager, "Update Dialog")
        }
        etPassword.setOnClickListener {
            dialogo.setTypSrc(3)
            dialogo.show(parentFragmentManager, "Update Dialog")
        }
    }

    private fun mostrarInformacionViewModel() {
        mostrarInfoViewModel.mostrar(prefs.getUserId())
        mostrarInfoViewModel.userNameLiveData.observe(this.viewLifecycleOwner, Observer {
            etUserName.apply {
                setText(it)
            }
        })
        mostrarInfoViewModel.numberLiveData.observe(this.viewLifecycleOwner, Observer {
            etNumber.apply {
                setText(it)
            }
        })
        mostrarInfoViewModel.passwordLiveData.observe(this.viewLifecycleOwner, Observer {
            etPassword.apply {
                setText(it)
            }
        })

    }
}