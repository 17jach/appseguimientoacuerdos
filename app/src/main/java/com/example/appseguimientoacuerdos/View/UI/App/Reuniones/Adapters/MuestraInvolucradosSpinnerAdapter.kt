package com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.appseguimientoacuerdos.Data.Database.Entities.InvolvedEntity
import com.example.appseguimientoacuerdos.R

class MuestraInvolucradosSpinnerAdapter(
    ctx:Context,
    val involvedList: List<String>
    ):ArrayAdapter<String>(ctx, 0,involvedList) {


    var list = involvedList

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, convertView, parent)
    }

    fun createItemView(position:Int, recycledView:View?, parent: ViewGroup):View{
        val involvedInfo = getItem(position)
        val view = recycledView?:LayoutInflater.from(parent.context).inflate(
            R.layout.item_spinner_list_involucrados,
            parent,
            false
        )

        view.findViewById<TextView>(R.id.tvInvolucradoSpinner).text = involvedInfo
        return view
    }


    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, convertView, parent)
    }




}