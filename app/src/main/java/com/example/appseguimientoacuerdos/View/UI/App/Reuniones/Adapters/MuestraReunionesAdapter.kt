package com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Data.Database.Entities.MeetingEntity
import com.example.appseguimientoacuerdos.R
import com.example.appseguimientoacuerdos.View.UI.App.Acuerdo.Fragments.MuestraAcuerdosFragment

class MuestraReunionesAdapter(
    val l:List<MeetingEntity>,
    val layout:Int,
    val fragmentManagerMuestraReuniones: FragmentManager
    )
    :RecyclerView.Adapter<MuestraReunionesAdapter.ViewHolder>(){

    var list = this.l as MutableList<MeetingEntity>

    class ViewHolder(val view:View, val fragmentManagerMuestraReuniones: FragmentManager):RecyclerView.ViewHolder(view) {
        lateinit var tvTitleReunion:TextView
        lateinit var cardContainer:CardView
        fun bind(meetingEntity: MeetingEntity) {
            tvTitleReunion = view.findViewById(R.id.tvTitleReunion)
            cardContainer = view.findViewById(R.id.cardContainerMeeting)

            tvTitleReunion.text = meetingEntity.title
            cardContainer.setOnClickListener {
                Component.replaceFragment(
                    fragmentManagerMuestraReuniones,
                    Component.REUNIONES_FRAGMENT_CONTAINER,
                    MuestraAcuerdosFragment( meetingEntity.idCategoryOwner, meetingEntity.idMeeting))
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ViewHolder(view, fragmentManagerMuestraReuniones)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(it: List<MeetingEntity>) {
        this.list = it  as MutableList<MeetingEntity>
    }

    fun getMeetingId(position: Int): Int {
        val currentMeeting = list[position]
        println("la función dentro del adapter recibe la position $position y el objeto que regresa es $currentMeeting")
        return currentMeeting.idMeeting
    }
    fun deleteMeeting(index:Int){
        println("El object que desilza es del index $index y pertence al involved ${this.list[index]}")

        this.list.removeAt(index)

        notifyDataSetChanged()
    }

}