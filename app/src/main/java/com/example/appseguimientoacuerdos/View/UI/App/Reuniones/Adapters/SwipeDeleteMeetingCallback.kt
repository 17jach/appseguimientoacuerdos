package com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Adapters

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

abstract class SwipeDeleteMeetingCallback: ItemTouchHelper.Callback() {
    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {

        var swipeFlags = ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT
        return ItemTouchHelper.Callback.makeMovementFlags(0, swipeFlags)

    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }
}