package com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Core.Prefs
import com.example.appseguimientoacuerdos.Data.Database.Entities.MeetingEntity
import com.example.appseguimientoacuerdos.R
import com.example.appseguimientoacuerdos.View.UI.App.Categorias.Fragments.MuestraCategoriasFragment
import com.example.appseguimientoacuerdos.View.UI.App.Involucrados.Adapters.SwipeDeleteMeetingCallback
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Adapters.MuestraReunionesAdapter
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Reuniones.MuestraReunionesViewModel
import com.example.appseguimientoacuerdos.databinding.FragmentMuestraReunionesBinding


class MuestraReunionesFragment(val categoryId: Int) : Fragment() {
    private var _binding:FragmentMuestraReunionesBinding?=null
    private val binding get() = _binding!!
    private lateinit var muestraReunionesViewModel:MuestraReunionesViewModel
    private var pressedTime: Long = 0

    /*Recycler*/

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMuestraReunionesBinding.inflate(layoutInflater, container, false)
        muestraReunionesViewModel = MuestraReunionesViewModel(requireActivity().application)
        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var meetingList = arrayListOf<MeetingEntity>()
        val adapter: MuestraReunionesAdapter = MuestraReunionesAdapter(meetingList, R.layout.item_list_reuniones, parentFragmentManager)
        val recylerReuniones:RecyclerView = binding.rvContainerReuniones
        val btnGoToNuevaReunionFragment = binding.btnGoToNuevaReunion


        btnGoToNuevaReunionFragment.setOnClickListener {
            Component.replaceFragment(
                parentFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                NuevaReunionFragment(categoryId, Component.RETURN_MEETING_ON_BACK_PRESSED)
            )
        }
        println("Category Id Value: $categoryId")
        muestraReunionesViewModel.isMeetingListEmpty(categoryId)
        muestraReunionesViewModel.getMeetingList(categoryId)

        muestraReunionesViewModel.hayReuniones.observe(this.viewLifecycleOwner, Observer{ hayReuniones ->
            if(!hayReuniones){
                Component.replaceFragment(parentFragmentManager, Component.REUNIONES_FRAGMENT_CONTAINER, SinReunionesFragment(categoryId))
            }
        })

        muestraReunionesViewModel.meetingList.observe(this.viewLifecycleOwner, Observer {
                adapter.updateList(it)
                adapter.notifyDataSetChanged()
        })
        recylerReuniones.adapter = adapter
        recylerReuniones.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)


        val swipeDeleteMeetingCallback = object : SwipeDeleteMeetingCallback(){
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                println("Position que regresa el viewHolder: $position")
                //adapter.deleteInvolved(position)
                //adapter.notifyItemRemoved(position)
                println("Id que regresa con el adapter.getInvolvedId: ${adapter.getMeetingId(position)}")
                muestraReunionesViewModel.deleteMeeting(adapter.getMeetingId(position))

                muestraReunionesViewModel.getMeetingList(categoryId)
                muestraReunionesViewModel.isMeetingListEmpty(categoryId)

            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeDeleteMeetingCallback)

        itemTouchHelper.attachToRecyclerView(recylerReuniones)


        requireActivity().onBackPressedDispatcher.addCallback(
            this.viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    Component.replaceFragment(
                        parentFragmentManager,
                        Component.REUNIONES_FRAGMENT_CONTAINER,
                        MuestraCategoriasFragment()
                    )
                }
            })

        /*
        * Para presione otra vez para cerrar
        * requireActivity().onBackPressedDispatcher.addCallback(
            this.viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (pressedTime + 2000 > System.currentTimeMillis()) {
                        requireActivity().finish();
                    } else {
                        Component.showToastMessage(requireContext(), "Presiona otra vez para salir")
                    }
                    pressedTime = System.currentTimeMillis();
                }
            })
        *
        *
        * */
    }

    private fun renderEmptyMeetingListView() {
        Component.replaceFragment(
            parentFragmentManager,
            Component.REUNIONES_FRAGMENT_CONTAINER,
            MuestraCategoriasFragment()
        )
    }


}