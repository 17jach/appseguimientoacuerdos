package com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Core.DatePickerHelper
import com.example.appseguimientoacuerdos.Data.Database.Entities.MeetingEntity
import com.example.appseguimientoacuerdos.View.UI.App.Categorias.Fragments.MuestraCategoriasFragment
import com.example.appseguimientoacuerdos.View.UI.App.Involucrados.Fragments.AgregarInvolucradoFragment
import com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Reuniones.NuevaReunionViewModel
import com.google.android.material.textfield.TextInputEditText
import java.util.*
import com.example.appseguimientoacuerdos.databinding.FragmentAgregarReunionBinding as FragmentNuevaReunionBinding


class NuevaReunionFragment(val categoryId: Int, val onBack: Boolean) : Fragment() {

    private var _binding: FragmentNuevaReunionBinding? = null
    private val binding get() = _binding!!

    /*Viewmodel*/
    private lateinit var nuevaReunionViewModel: NuevaReunionViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentNuevaReunionBinding.inflate(inflater, container, false)
        nuevaReunionViewModel = NuevaReunionViewModel(requireActivity().application)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val btnAgregarReunion = binding.btnAgregarReunion

        val tituloReunion = binding.textInputEditTituloReunion
        val orderdayReunion = binding.textInputEditOrdenDiaReunion
        val calendarPicker = binding.textInputLayoutDatePicker

        var dateFinishFromFragment: String = getTodayDate()
        setTodayDateOnDatePicker(calendarPicker)


        calendarPicker.setOnClickListener {
            try {
                showDatePicker()
            } catch (e: Exception) {
                println("Error: ${e.message}")
            }
        }

        nuevaReunionViewModel.finishDateFromDatePicker.observe(
            this.viewLifecycleOwner,
            androidx.lifecycle.Observer {
                dateFinishFromFragment = it
                calendarPicker.apply {
                    setText(dateFinishFromFragment)
                }
            })


        btnAgregarReunion.setOnClickListener {
            //Component.showToastMessage(requireContext(), "Agregar reunion")
            //nuevaReunionViewModel.getMeetingFromUserId(Prefs(requireContext()).getUserId())
            //Logica para la transacción de agregar la reunion
            val nuevaReunion = MeetingEntity(
                0,
                tituloReunion.text.toString(),
                orderdayReunion.text.toString(),
                dateFinishFromFragment,
                categoryId
            )


                nuevaReunionViewModel.insertarMeeting(parentFragmentManager,nuevaReunion)


        }


        if (onBack == Component.RETURN_CATEGORY_ON_BACK_PRESSED) {
            requireActivity().onBackPressedDispatcher.addCallback(
                this.viewLifecycleOwner,
                object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {
                        Component.replaceFragment(
                            parentFragmentManager,
                            Component.REUNIONES_FRAGMENT_CONTAINER,
                            MuestraCategoriasFragment()
                        )
                    }
                })
        } else if (onBack == Component.RETURN_MEETING_ON_BACK_PRESSED) {
            requireActivity().onBackPressedDispatcher.addCallback(
                this.viewLifecycleOwner,
                object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {
                        Component.replaceFragment(
                            parentFragmentManager,
                            Component.REUNIONES_FRAGMENT_CONTAINER,
                            MuestraReunionesFragment(categoryId)
                        )
                    }
                })
        }
    }

    private fun setTodayDateOnDatePicker(calendarPicker: TextInputEditText) {
        val date = getTodayDate()
        calendarPicker.apply {
            setText(date)
        }
    }

    fun getTodayDate(): String {
        val c = Calendar.getInstance()
        val day = c.get(Calendar.DAY_OF_MONTH)
        val month = c.get(Calendar.MONTH)
        val year = c.get(Calendar.YEAR)

        val correctMonth = getCorrectMonth(month)

        return "$day/$correctMonth/$year"
    }


    private fun showDatePicker() {
        val datePicker = DatePickerHelper { day, month, year ->
            onDateSelected(day, month, year)
        }
        datePicker.show(parentFragmentManager, "DatePicker")
    }

    private fun onDateSelected(day: Int, month: Int, year: Int) {
        val monthCorrect = getCorrectMonth(month)
        val dateInText = "$day/$monthCorrect/$year"
        nuevaReunionViewModel.setFinishDateFromDatePicker(dateInText)
    }

    private fun getCorrectMonth(month: Int): String {
        return when (month) {
            0 -> return "Enero"
            1 -> return "Febrero"
            2 -> return "Marzo"
            3 -> return "Abril"
            4 -> return "Mayo"
            5 -> return "Junio"
            6 -> return "Julio"
            7 -> return "Agosto"
            8 -> return "Septiembre"
            9 -> return "Octubre"
            10 -> return "Noviembre"
            11 -> return "Diciembre"
            else -> {
                return ""
            }
        }
    }
}