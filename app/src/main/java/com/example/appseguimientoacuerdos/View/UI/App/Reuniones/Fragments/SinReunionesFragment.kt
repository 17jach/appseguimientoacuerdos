package com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.View.UI.App.Categorias.Fragments.MuestraCategoriasFragment
import com.example.appseguimientoacuerdos.databinding.FragmentSinReunionesBinding


class SinReunionesFragment(val categoryId:Int) : Fragment() {

    /*binding*/
    private var _binding:FragmentSinReunionesBinding? = null
    private val binding get() = _binding!!
    private var pressedTime: Long = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSinReunionesBinding.inflate(inflater, container, false )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnNuevaReunion.setOnClickListener {
            Component.replaceFragment(
                parentFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                NuevaReunionFragment(categoryId, Component.RETURN_MEETING_ON_BACK_PRESSED) )
        }



        requireActivity().onBackPressedDispatcher.addCallback(this.viewLifecycleOwner, object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                Component.replaceFragment(
                    parentFragmentManager,
                    Component.REUNIONES_FRAGMENT_CONTAINER,
                    MuestraCategoriasFragment()
                )
            }
        })
    }

}