package com.example.appseguimientoacuerdos.View.UI.App.Reuniones

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Core.ObjPrefs.Companion.prefs
import com.example.appseguimientoacuerdos.R
import com.example.appseguimientoacuerdos.View.UI.App.Categorias.Fragments.MuestraCategoriasFragment
import com.example.appseguimientoacuerdos.View.UI.App.MyInfo.Fragments.MyInformacion
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Fragments.MuestraReunionesFragment
import com.example.appseguimientoacuerdos.View.UI.Login.Login
import com.example.appseguimientoacuerdos.databinding.ActivityReunionesBinding
import com.google.android.material.navigation.NavigationView

class Reuniones : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    /*Binding*/
    private lateinit var binding: ActivityReunionesBinding

    /*Nav drawer*/
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var toggle: ActionBarDrawerToggle




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReunionesBinding.inflate(layoutInflater)
        setContentView(binding.root)


        /*CONFIGURACION DEL DRAWER NAV VIEW*/

        /*Toolbar buscamos la toolbar que hicimos*/
        val toolbar: Toolbar = findViewById(R.id.toolbar_main)
        setSupportActionBar(toolbar)
        drawerLayout = binding.drawerLayout
        toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_closed
        )
        drawerLayout.addDrawerListener(toggle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu_fff)
        supportActionBar?.setTitle(R.string.app_name)
        val navigationView: NavigationView = binding.navView
        navigationView.setNavigationItemSelectedListener(this)
        Component.loadFragmentHome(
            supportFragmentManager,
            Component.REUNIONES_FRAGMENT_CONTAINER,
            MuestraCategoriasFragment()
        )
    }



    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            //El click en reuniones será más complejo por la llamada a la base de
            // datos para ver si hay o no hay reuniones

            R.id.nav_item_categorias -> Component.replaceFragment(
                supportFragmentManager,
                Component.REUNIONES_FRAGMENT_CONTAINER,
                MuestraCategoriasFragment()
            )

            R.id.nav_item_reuniones -> {
                Component.replaceFragment(
                    supportFragmentManager,
                    Component.REUNIONES_FRAGMENT_CONTAINER,
                    MuestraCategoriasFragment()
                )
                Component.showToastMessage(applicationContext, "Selecciona una categoria")

            }


            R.id.nav_item_acuerdos -> {
                Component.replaceFragment(
                    supportFragmentManager,
                    Component.REUNIONES_FRAGMENT_CONTAINER,
                    MuestraCategoriasFragment()
                )
                Component.showToastMessage(applicationContext, "Selecciona una categoria")
            }

            R.id.nav_item_miinfo -> {
                Component.replaceFragment(
                    supportFragmentManager,
                    Component.REUNIONES_FRAGMENT_CONTAINER,
                    MyInformacion()
                )
            }

            R.id.nav_item_cerrarsesion -> {
                startActivity(Intent(applicationContext, Login::class.java))
                prefs.clearInfo()
                finish()
            }

        }

        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onPostCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onPostCreate(savedInstanceState, persistentState)
        toggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        toggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}