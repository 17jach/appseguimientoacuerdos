package com.example.appseguimientoacuerdos.View.UI.Login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.appseguimientoacuerdos.R
import com.example.appseguimientoacuerdos.databinding.ActivityForgotPasswordBinding

class ForgotPassword : AppCompatActivity() {
    private lateinit var binding: ActivityForgotPasswordBinding

    //VIEWMODEL
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)


    }
}