package com.example.appseguimientoacuerdos.View.UI.Login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Core.ObjPrefs.Companion.prefs
import com.example.appseguimientoacuerdos.View.UI.App.Reuniones.Reuniones
import com.example.appseguimientoacuerdos.View.ViewModel.LoginViewModel.LoginViewModel
import com.example.appseguimientoacuerdos.databinding.ActivityLoginBinding
import com.google.android.material.button.MaterialButton
import com.google.android.material.textview.MaterialTextView

class Login : AppCompatActivity() {
    //Preparamos el binding
    private lateinit var binding: ActivityLoginBinding

    //Preparamos el viewmodel
    private lateinit var loginViewModel: LoginViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        loginViewModel = LoginViewModel(application)
        initTables()

        //Check Login prefs
        if(prefs.getIsLogged()){
            val meetingActivity = Intent(applicationContext, Reuniones::class.java)
            startActivity(meetingActivity);
            finish()
        }


        //Declaramos los componentes
        var btnLogin: MaterialButton = binding.btnLogin
        var btnSignup: MaterialTextView = binding.tvGoSignup


        ///Click de los botones
        btnSignup.setOnClickListener {
            val signupActivity = Intent(applicationContext, SignUp::class.java)
            run {
                startActivity(signupActivity)
                this.finish()
            }
        }

        loginViewModel.isRegistred.observe(this, Observer { userIsRegistred ->
            if(userIsRegistred) {
                //Save login session on into shared preferences
                prefs.saveIsLogged(true)
                loginViewModel.userId.observe(this, Observer { id ->
                    //Save userId logged into shared preferences
                    prefs.saveUserId(id)
                })
                startActivity(Intent(applicationContext, Reuniones::class.java))
                finish()
            }else{
                Component.showToastMessage(applicationContext, "Usuario no encontrado")
            }
        })

        btnLogin.setOnClickListener {
            run {
                val user = binding.textInputEditUsername.text.toString()
                val password = binding.textInputEditPassword.text.toString()
                loginViewModel.authUser(user, password)
            }
        }
    }

    private fun initTables() {
        loginViewModel.initPriorityTable()
        loginViewModel.initStatusTable()
        loginViewModel.initTypeTable()
    }
}