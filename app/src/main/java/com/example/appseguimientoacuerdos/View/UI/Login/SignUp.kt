package com.example.appseguimientoacuerdos.View.UI.Login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Data.Database.Entities.UserEntity
import com.example.appseguimientoacuerdos.View.ViewModel.LoginViewModel.SignupViewModel
import com.example.appseguimientoacuerdos.databinding.ActivitySignUpBinding

class SignUp : AppCompatActivity() {
    private lateinit var binding: ActivitySignUpBinding
    private lateinit var signupViewModel: SignupViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        signupViewModel = SignupViewModel(application)

        val btnLogin = binding.tvGoLogin
        val btnSignup = binding.btnSignup

        btnSignup.setOnClickListener {
            val username = binding.textInputEditUsername.text.toString()
            val password = binding.textInputEditPassword.text.toString()
            val confirmPassword = binding.textInputConfirmPassword.text.toString()
            val number = binding.textInputEditNumber.text.toString()

            val userEntity = UserEntity(0, username, number, password)

            if (Component.validateSignupInput(username, password, number)) {
                if (password.equals(confirmPassword)) {
                    signupViewModel.registrarUsuario(userEntity)
                } else {
                    Component.showToastMessage(
                        applicationContext,
                        "La contraseñas no coinciden"
                    )
                }
            } else {
                Component.showToastMessage(
                    applicationContext,
                    "Verifique la entrada de datos"
                )
            }
        }

        signupViewModel.sePuedeRegistrar.observe(this, Observer { sePuedeRegistrar ->
            if (sePuedeRegistrar){
                Component.showToastMessage(applicationContext, "Se ha registrado correctamente")
                startActivity(Intent(applicationContext, Login::class.java))
                finish()
            } else{
                Component.showToastMessage(applicationContext, "Ya hay un usuario con este nombre")
            }
        })

        btnLogin.setOnClickListener {
            val loginActivity = Intent(applicationContext, Login::class.java)
            run{
                startActivity(loginActivity)
                this.finish()
            }
        }
    }
}