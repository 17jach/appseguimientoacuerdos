package com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Acuerdos

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Data.Database.Entities.AgreementEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.InvolvedEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.ResponsableEntity
import com.example.appseguimientoacuerdos.Domain.AgreementUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AgregarAcuerdosViewModel(application: Application) : ViewModel() {
    var agreementUseCase = AgreementUseCase(application)
    var involvedListMutableList: MutableList<InvolvedEntity> = arrayListOf<InvolvedEntity>()
    var finishDateFromDatePicker = MutableLiveData<String>()
    var priorityDataList = MutableLiveData<List<String>>()
    var statusDataList = MutableLiveData<List<String>>()



    private var number: String = ""
    private var description: String = ""
    private var dateCreate: String = ""
    private var dateFinish: String = ""
    private var prioridad: Int = Component.PRIORIDAD_NORMAL
    private var prioridadText = ""
    private var status: Int = Component.STATUS_EN_PROGRESO
    private var statusText = ""
    private var idMeetingOwner = 0


    val involvedByMeetingList = MutableLiveData<List<InvolvedEntity>>()


    /*Para responsables*/
    private var idCurrentAgreement:Int=0
    val closeDialogMLD = MutableLiveData<Boolean>()
    val responsableListMLD = MutableLiveData<List<ResponsableEntity>>()

    fun getResponsables(idAgreement:Int){
        viewModelScope.launch(Dispatchers.IO) {
            println("Result de get en viewModel: ${agreementUseCase.getResponsableByAgreement(idAgreement)}")
            responsableListMLD.postValue(agreementUseCase.getResponsableByAgreement(idAgreement))
            closeDialogMLD.postValue(true)
        }
    }

    fun addResponsable(responsable:ResponsableEntity){
        viewModelScope.launch(Dispatchers.IO){
            val id = agreementUseCase.insertResponsable(responsable)
            if(id!=null){
                getResponsables(getIdCurrentAgreement())
            }
        }
    }


    /*Not use magic words*/
    fun setPrioridadAgreement(priorityInput: String) {
        when (priorityInput) {
            "Critica" -> prioridad = Component.PRIORIDAD_CRITICA
            "Importante" -> prioridad = Component.PRIORIDAD_IMPORTANTE
            "Normal" -> prioridad = Component.PRIORIDAD_NORMAL
        }
    }
    fun getPrioridadAgreement(): String {
        when(prioridad){
             Component.PRIORIDAD_CRITICA -> prioridadText = "Critica"
             Component.PRIORIDAD_IMPORTANTE -> prioridadText = "Importante"
             Component.PRIORIDAD_NORMAL -> prioridadText = "Normal"
        }
        return prioridadText
    }


    fun setStatusAgreement(statusInput: String) {
        when (statusInput) {
            "Pendiente" -> status = Component.STATUS_PENDIENTE
            "En progreso" -> status = Component.STATUS_EN_PROGRESO
            "Terminado" -> status = Component.STATUS_TERMINADO
        }
    }
    fun getStatusAgreement(): String {
        when(status){
            Component.STATUS_PENDIENTE -> statusText = "Pendiente"
            Component.STATUS_EN_PROGRESO -> statusText = "En progreso"
            Component.STATUS_TERMINADO -> statusText = "Terminado"
        }
        return statusText
    }



    fun setFinishDateFromDatePicker(date: String) {
        viewModelScope.launch(Dispatchers.IO) {
            finishDateFromDatePicker.postValue(date)
        }
    }


    /*
    Parte uno
     */
    fun setNumberAgreement(nameInput: String) {
        number = nameInput
    }

    fun getNumberAgreement(): String {
        return this.number
    }

    fun setDescriptionAgreement(descriptionInput: String) {
        description = descriptionInput
    }

    fun getDescription(): String {
        return this.description
    }

    fun setIdMeetingOwner(idMeeting: Int) {
        idMeetingOwner = idMeeting
    }

    fun getIdMeetingOwner(): Int {
        return this.idMeetingOwner
    }

    fun setCreateDate(date: String) {
        dateCreate = date
    }

    fun getCreateDate(): String {
        return this.dateCreate
    }

    fun getFinishDate(): String {
        return this.dateFinish
    }

    fun getPrioridad(): Int {
        return this.prioridad
    }
    fun getStatus(): Int {
        return this.status
    }


    /*
    * End parte uno
    * */

    /*PARTE DOS*/
    fun setFinishDateFromFragment(dateFinishFromFragment: String) {
        dateFinish = dateFinishFromFragment
    }

    fun getAllInvolvedByMeeting(idMeetingOwner: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            involvedByMeetingList.postValue(agreementUseCase.getInvolvedByMeeting(idMeetingOwner))

        }
    }


    fun insertAcuerdo(currentAgreement:AgreementEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            val id = agreementUseCase.insertAcuerdo(currentAgreement)
            setIdCurrentAgreement(id)
        }
    }

    fun setIdCurrentAgreement(id:Int){
        this.idCurrentAgreement = id
    }

    fun getIdCurrentAgreement(): Int {
        return this.idCurrentAgreement
    }

    fun getAllStatusData() {
        viewModelScope.launch(Dispatchers.IO) {
            var listStatusText = arrayListOf<String>()
            for (status in agreementUseCase.getAllStatusData()) {
                listStatusText.add(status.descriptionStatus)
            }
            statusDataList.postValue(listStatusText)
        }
    }

    fun getAllPriorityData() {
        viewModelScope.launch() {
            //println("Info data prioridad: ${agreementUseCase.getAllPriorityData()}")
            var listPriorityText = arrayListOf<String>()
            for (prioridad in agreementUseCase.getAllPriorityData()) {
                listPriorityText.add(prioridad.descriptionPriority)
            }
            priorityDataList.postValue(listPriorityText)
        }
    }



}