package com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Acuerdos

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Data.Repository.Response.AgreementRecyclerResponse
import com.example.appseguimientoacuerdos.Domain.AgreementUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MuestraAcuerdosViewModel(val application: Application) : ViewModel() {
    val acuerdosUseCase = AgreementUseCase(application)
    val acuerdosResponseList = MutableLiveData<List<AgreementRecyclerResponse>>()
    val isAcuerdosListEmpty = MutableLiveData<Boolean>()

    val filtroMLD = MutableLiveData<String>()

    fun setFiltro(filtro:String){
        filtroMLD.postValue(filtro)
    }

    suspend fun getAgreementWithInfoCall(idMeetingOwner:Int, filtro:String): List<AgreementRecyclerResponse> {
        var listResult= emptyList<AgreementRecyclerResponse>()

        when(filtro){
            Component.FILTRO_TODOS_LOS_ACUERDOS_TEXTO->{
                listResult = acuerdosUseCase.getAgreementWithMeetingInfo(idMeetingOwner,Component.FILTRO_TODOS_LOS_ACUERDOS)
            }
            Component.FILTRO_EN_PROCESO_TEXTO->{
                listResult = acuerdosUseCase.getAgreementWithMeetingInfo(idMeetingOwner,Component.FILTRO_EN_PROCESO)
            }
            Component.FILTRO_PENDIENTES_TEXTO->{
                listResult = acuerdosUseCase.getAgreementWithMeetingInfo(idMeetingOwner,Component.FILTRO_PENDIENTES)
            }
            Component.FILTRO_TERMINADOS_TEXTO->{
                listResult = acuerdosUseCase.getAgreementWithMeetingInfo(idMeetingOwner,Component.FILTRO_TERMINADOS)
            }
            Component.FILTRO_PRIORIDAD_TEXTO->{
                listResult = acuerdosUseCase.getAgreementWithMeetingInfo(idMeetingOwner,Component.FILTRO_PRIORIDAD)
            }
        }
        return listResult
    }

    fun getAllAcuerdosResponse(idMeetingOwner: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val listResult= getAgreementWithInfoCall(idMeetingOwner, filtroMLD.value!!)

            if (listResult.isEmpty()) {
                //Valida si realmente no hay acuerdos o si solo en ese filtro
                if(acuerdosUseCase.getAgreementWithMeetingInfo(idMeetingOwner,1).isEmpty()){
                    isAcuerdosListEmpty.postValue(true)
                }else{
                    val list = arrayListOf<AgreementRecyclerResponse>()
                    isAcuerdosListEmpty.postValue(false)
                    acuerdosResponseList.postValue(list)

                }
            } else {
                isAcuerdosListEmpty.postValue(false)
                acuerdosResponseList.postValue(listResult)

            }
        }
    }

    fun deleteAgreement(agreementId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            acuerdosUseCase.deleteAgreement(agreementId)
        }
    }


}