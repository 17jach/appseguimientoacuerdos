package com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Acuerdos

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appseguimientoacuerdos.Data.Database.Entities.AgreementEntity
import com.example.appseguimientoacuerdos.Domain.AgreementUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MuestraDetallesAcuerdoViewModel(application: Application):ViewModel() {
    val agreementUseCase = AgreementUseCase(application)
    var infoAgreementRespone = MutableLiveData<AgreementEntity>()
    var priorityDataList = MutableLiveData<List<String>>()
    var statusDataList = MutableLiveData<List<String>>()


    fun getAgreementById(idAgreement:Int){
        viewModelScope.launch(Dispatchers.IO){
            infoAgreementRespone.postValue(agreementUseCase.getAgreementById(idAgreement))
        }
    }
    fun getAllStatusData() {
        viewModelScope.launch(Dispatchers.IO) {
            var listStatusText = arrayListOf<String>()
            for (status in agreementUseCase.getAllStatusData()) {
                listStatusText.add(status.descriptionStatus)
            }
            statusDataList.postValue(listStatusText)
        }
    }

    fun getAllPriorityData() {
        viewModelScope.launch() {
            //println("Info data prioridad: ${agreementUseCase.getAllPriorityData()}")
            var listPriorityText = arrayListOf<String>()
            for (prioridad in agreementUseCase.getAllPriorityData()) {
                listPriorityText.add(prioridad.descriptionPriority)
            }
            priorityDataList.postValue(listPriorityText)
        }
    }
}