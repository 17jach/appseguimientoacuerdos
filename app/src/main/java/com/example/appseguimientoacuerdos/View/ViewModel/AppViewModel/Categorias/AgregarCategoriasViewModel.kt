package com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Categorias

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appseguimientoacuerdos.Domain.CategoryUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AgregarCategoriasViewModel(val application: Application):ViewModel() {
    private val categoryUseCase = CategoryUseCase(application)


    fun agregarCategoria(tituloCategoria: String, idUserOwner:Int) {
        viewModelScope.launch(Dispatchers.IO){
            categoryUseCase.agregarCategoria(tituloCategoria, idUserOwner)
        }
    }


}