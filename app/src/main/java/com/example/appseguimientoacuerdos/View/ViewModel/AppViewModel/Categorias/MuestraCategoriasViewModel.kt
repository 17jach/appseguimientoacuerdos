package com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Categorias

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appseguimientoacuerdos.Data.Database.Entities.CategoryEntity
import com.example.appseguimientoacuerdos.Domain.CategoryUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MuestraCategoriasViewModel(val application: Application) : ViewModel() {
    private val categoryUseCase = CategoryUseCase(application)
    val categorysList = MutableLiveData<List<CategoryEntity>>()

    fun getCategoryList(userId: Int) {
        viewModelScope.launch(Dispatchers.IO){
            categorysList.postValue(categoryUseCase.getCategoryList(userId))
        }
    }

    fun deleteMeeting(categoryId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            categoryUseCase.deleteCategory(categoryId)
        }
    }


}