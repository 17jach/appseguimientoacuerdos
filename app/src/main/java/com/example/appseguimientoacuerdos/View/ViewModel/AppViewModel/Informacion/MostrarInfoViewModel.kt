package com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Informacion

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appseguimientoacuerdos.Domain.InformationUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MostrarInfoViewModel(application: Application):ViewModel() {
    val informationUseCase = InformationUseCase(application)
    val userNameLiveData = MutableLiveData<String>()
    val numberLiveData = MutableLiveData<String>()
    val passwordLiveData = MutableLiveData<String>()

    fun mostrar(id: Int) {
        viewModelScope.launch(Dispatchers.IO){
            val userName = informationUseCase.getUserName(id)
            val number = informationUseCase.getNumber(id)
            val password = informationUseCase.getPassword(id)
            userNameLiveData.postValue(userName)
            numberLiveData.postValue(number)
            passwordLiveData.postValue(password)
        }
    }


    fun updateInfoByTyp(typSrc: Int, txtInput: String, userId: Int) {
        when(typSrc){
            1->setUserName(txtInput, userId)
            2->setNumber(txtInput, userId)
            3->setPassword(txtInput, userId)
        }
    }


    fun setUserName(userName: String, userId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            informationUseCase.setUserNameUpdate(userName,userId)
        }
    }

    fun setNumber(numberHardCode: String, userId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            informationUseCase.setNumberUpdate(numberHardCode, userId) }
    }

    fun setPassword(passwordHardCode: String, userId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            informationUseCase.setPasswordUpdate(passwordHardCode, userId)
        }
    }

    fun getUsernameLiveData(): String {
        return this.userNameLiveData.value!!
    }
    fun getNumberLiveData(): String {
        return this.numberLiveData.value!!
    }
    fun getPasswordLiveData(): String {
        return this.passwordLiveData.value!!
    }


}