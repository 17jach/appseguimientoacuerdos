package com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Involucrados

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appseguimientoacuerdos.Data.Database.Entities.InvolvedEntity
import com.example.appseguimientoacuerdos.Domain.InvolvedUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AgregarInvolucradosViewModel(val application: Application):ViewModel() {

    val involvedUseCase = InvolvedUseCase(application)
    val listAllInvolved = MutableLiveData<List<InvolvedEntity>>()

    val involvedById = MutableLiveData<InvolvedEntity>()

    fun insertInvolved(involvedEntity: InvolvedEntity){
        viewModelScope.launch(Dispatchers.IO) {
            involvedUseCase.insertInvolved(involvedEntity)
        }
    }

    fun getAllInvolvedByMeeting(idMeetingOwner:Int){
        viewModelScope.launch(Dispatchers.IO){
            val list = involvedUseCase.getAllInvolvedByMeeting(idMeetingOwner)
            listAllInvolved.postValue(list)
        }
    }

    fun deleteInvolved(involvedId:Int){
        viewModelScope.launch(Dispatchers.IO){
            involvedUseCase.deleteInvolved(involvedId)
        }
    }

    fun getInvolvedById(involvedId: Int) {
        viewModelScope.launch(Dispatchers.IO){
            involvedById.postValue(involvedUseCase.getInvolvedById(involvedId))
        }
    }

    fun updateInvolved(currentInvolved: InvolvedEntity) {
        viewModelScope.launch(Dispatchers.IO){
            val isUpdated = involvedUseCase.updateInvolved(currentInvolved)
            if(isUpdated ==1){
                println("actualizado correctamente")
            }else{
                println("Ocurrio un error al actualizar")
            }
        }

    }


}