package com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Reuniones

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appseguimientoacuerdos.Data.Database.Entities.MeetingEntity
import com.example.appseguimientoacuerdos.Domain.MeetingUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MuestraReunionesViewModel(application: Application) : ViewModel() {
    val meetingList = MutableLiveData<List<MeetingEntity>>()
    val meetingUseCase = MeetingUseCase(application)
    val hayReuniones = MutableLiveData<Boolean>()

    fun isMeetingListEmpty(categoryId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val list = meetingUseCase.getMeetingFromCategoryId(categoryId)
            if (list.isEmpty()) {
                hayReuniones.postValue(false)
            } else {
                hayReuniones.postValue(true)
            }
        }
    }


    fun getMeetingList(categoryId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            /*val meetingsTest = listOf<MeetingEntity>(
                MeetingEntity(1,"Test hardcode 1", "Subject test hardcode 1", 1),
                MeetingEntity(2,"Test hardcode 2", "Subject test hardcode 2", 1),
                MeetingEntity(3,"Test hardcode 3", "Subject test hardcode 3", 1),
                MeetingEntity(4,"Test hardcode 4", "Subject test hardcode 4", 1),
                MeetingEntity(5,"Test hardcode 5", "Subject test hardcode 5", 1),
                MeetingEntity(6,"Test hardcode 6", "Subject test hardcode 6", 1),
                MeetingEntity(7,"Test hardcode 7", "Subject test hardcode 7", 1)
            )

             */

            /*****
             * Vamos a cambiar entre las dos siguientes lineas para hacer pruebas entre hardcodeado y
             * ******/
            val meetingListFromUseCase: List<MeetingEntity> = meetingUseCase.getMeetingFromCategoryId(categoryId)
            //val meetingListFromUseCase = emptyList<MeetingEntity>()

            if (meetingListFromUseCase.isNotEmpty()) {
                meetingList.postValue(meetingListFromUseCase)
                println("Lista no vacia en viewmodel")
                println(meetingList)
            } else {
                println("Lista vacia en viewmodel")
            }
        }
    }

    fun deleteMeeting(meetingId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            meetingUseCase.deleteMeeting(meetingId)
        }

    }
}