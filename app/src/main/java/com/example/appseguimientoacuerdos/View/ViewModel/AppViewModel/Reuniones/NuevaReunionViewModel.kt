package com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Reuniones

import android.app.Application
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appseguimientoacuerdos.Core.Component
import com.example.appseguimientoacuerdos.Data.Database.Entities.InvolvedEntity
import com.example.appseguimientoacuerdos.Data.Database.Entities.MeetingEntity
import com.example.appseguimientoacuerdos.Domain.MeetingUseCase
import com.example.appseguimientoacuerdos.View.UI.App.Involucrados.Fragments.AgregarInvolucradoFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NuevaReunionViewModel(application: Application):ViewModel() {

    val meetingUseCase = MeetingUseCase(application)
    val meetingList = MutableLiveData<List<MeetingEntity>>()
    var dateMeeting = ""
    val meetingIdCreated = MutableLiveData<Int>()

    /*Lo del date*/
    var finishDateFromDatePicker = MutableLiveData<String>()

    fun setFinishDateFromDatePicker(date: String) {
        viewModelScope.launch(Dispatchers.IO) {
            finishDateFromDatePicker.postValue(date)
        }
    }

    fun setFinishDateFromFragment(dateFinishFromFragment: String) {
        viewModelScope.launch(Dispatchers.IO) {
            dateMeeting = dateFinishFromFragment
        }
    }

    fun insertarMeeting(fragmentManager: FragmentManager, meeting: MeetingEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            setMeetingIdCreated(fragmentManager ,meetingUseCase.insertarReunion(meeting), meeting.idCategoryOwner)
        }
    }

    
    fun setMeetingIdCreated(parentFragmentManager: FragmentManager, id:Int, categoryOwner:Int){
        println("Id created vm: $id")
        this.meetingIdCreated.postValue(id)
        onInsertSuccess(parentFragmentManager, id, categoryOwner)

    }

    fun onInsertSuccess(parentFragmentManager:FragmentManager, meetingIdCreated:Int, categoryOwner:Int){

        Component.replaceFragment(
            parentFragmentManager, Component.REUNIONES_FRAGMENT_CONTAINER,
            AgregarInvolucradoFragment(meetingIdCreated, categoryOwner)
        )
    }

    fun getMeetingFromUserId(userId:Int){
        viewModelScope.launch(Dispatchers.IO) {
           // var meetingListF = meetingUseCase.getMeetingFromUserId(userId)
            var meetingListF = emptyList<MeetingEntity>()
            meetingList.postValue(meetingListF)
        }
    }



}