package com.example.appseguimientoacuerdos.View.ViewModel.AppViewModel.Reuniones

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appseguimientoacuerdos.Domain.MeetingUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ReunionesViewModel(val application: Application):ViewModel() {
    //val list= MutableLiveData<List<MeetingEntity>>()
    val hayReuniones = MutableLiveData<Boolean>()
    val meetingUseCase = MeetingUseCase(application)




    fun revisarSiHayReuniones(userId:Int){
        viewModelScope.launch(Dispatchers.IO){
            val listaReuniones = meetingUseCase.getMeetingFromCategoryId(userId)



            /*Hardcode para test del fragment que inicia*/
            /*
            var reunionesList: ArrayList<MeetingEntity> = arrayListOf()
            reunionesList.add(
                MeetingEntity(
                    0,
                    "Title meetin 1",
                    "Subject  meeting 1",
                    Prefs(application).getUserId()
                )
            )
            val listaReuniones =reunionesList

             */



            if (listaReuniones.isNotEmpty()) {
                hayReuniones.postValue(true)
            } else {
                hayReuniones.postValue(false)
            }
        }
    }
}