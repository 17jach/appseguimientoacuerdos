package com.example.appseguimientoacuerdos.View.ViewModel.LoginViewModel

import android.app.Application
import androidx.lifecycle.*
import com.example.appseguimientoacuerdos.Domain.LoginUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel(application: Application): ViewModel() {

    val isRegistred = MutableLiveData<Boolean>()
    val useCaseLogin:LoginUseCase = LoginUseCase(application)
    val userId=MutableLiveData<Int>()

    fun authUser(user: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            if (useCaseLogin.authUser(user, password)){
                getUserId(user, password)
                isRegistred.postValue(true)
            }else{
                isRegistred.postValue(false)
            }
        }
    }

    fun getUserId(user:String, password:String) {
        viewModelScope.launch(Dispatchers.IO) {
            userId.postValue(useCaseLogin.getUserId(user, password))
        }
    }

    fun initPriorityTable() {
        viewModelScope.launch(Dispatchers.IO) {
            useCaseLogin.initPriorityTable()
        }
    }

    fun initStatusTable() {
        viewModelScope.launch(Dispatchers.IO) {
            useCaseLogin.initStatusTable()
        }
    }

    fun initTypeTable() {
        viewModelScope.launch(Dispatchers.IO) {
            useCaseLogin.initTypeTable()
        }
    }




}