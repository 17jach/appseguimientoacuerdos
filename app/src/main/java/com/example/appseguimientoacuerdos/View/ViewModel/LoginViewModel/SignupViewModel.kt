package com.example.appseguimientoacuerdos.View.ViewModel.LoginViewModel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appseguimientoacuerdos.Data.Database.Entities.UserEntity
import com.example.appseguimientoacuerdos.Domain.SignupUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SignupViewModel(application: Application): ViewModel() {
    val signupUseCase = SignupUseCase(application)
    val sePuedeRegistrar = MutableLiveData<Boolean>()

    fun registrarUsuario(user: UserEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            if (signupUseCase.registrarUsuario(user)) {
                sePuedeRegistrar.postValue(true)
            } else {
                sePuedeRegistrar.postValue(false)
            }
        }
    }
}